﻿using IZY.SlySEO.Data.DB;
using IZY.SlySEO.Data.Entities;
using IZY.SlySEO.Models.Google;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Linq;

namespace IZY.SlySEO.Web.Controllers
{
	public class SearchIntentController : Controller
    {
        // GET: SearchIntent
        public ActionResult Index()
        {
            return View();
        }

		public object GetData(string searchTerm)
		{
			var result = new List<SearchIntentResult>();

			using (SlySEOEntities db = new SlySEOEntities())
			{
				var searchIntentCrawlingId = DBSearchIntentCrawlings.GetId(searchTerm);

				result = (from sic in db.SearchIntentCrawlings
						  join t in db.Terms on sic.TermId equals t.Id
						  join si in db.SearchIntents on sic.SearchIntentId equals si.Id
						  where sic.Id == searchIntentCrawlingId
						  select new SearchIntentResult
						  {
							  SearchTerm = t.Value,
							  AnalyzedUrlsCount = sic.AnalyzedUrlsCount,
							  AnalyzedUrls = (from sici in db.SearchIntentCrawlingItems
											  join si in db.SearchIntents on sici.SearchIntentId equals si.Id
											  where sici.SearchIntentCrawlingId == searchIntentCrawlingId
											  select new SearchIntentItem
											  {
												  Url = sici.Url,
												  Position = sici.Position,
												  InformationalScore = sici.InformationalScore,
												  NavigationalScore = sici.NavigationalScore,
												  TransactionalScore = sici.TransactionalScore,
												  Winner = (SearchIntentType)si.Id
											  }).ToList(),
							  InformationalScore = sic.InformationalScore,
							  NavigationalScore = sic.NavigationalScore,
							  TransactionalScore = sic.TransactionalScore,
							  ProbabilityPercentage = sic.ProbabilityPercentage,
							  Winner = (SearchIntentType)si.Id
						  }).ToList();
			}


			return Json(result, JsonRequestBehavior.AllowGet);

		}
    }
}