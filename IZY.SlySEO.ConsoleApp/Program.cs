﻿using IZY.SlySEO.Clients.Google;
using IZY.SlySEO.Clients.OnPage;
using IZY.SlySEO.Utilities.Analyzers;
using IZY.SlySEO.Utilities.Logging;
using log4net;
using System.Collections.Generic;
using System.Linq;
using IZY.SlySEO.Data.DB;
using IZY.SlySEO.Clients.Wappalyzer;
using IZY.SlySEO.Models.Google;

namespace IZY.SlySEO.ConsoleApp
{
	class Program
    {
		private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		static void Main(string[] args)
        {
            Log4ViewConfig.ConfigureUdp();

			SearchIntentClient("real fahrrad");


		}

		static void SerpClient(string searchTerm, int snippetCount)
		{
            new SerpClient().Get(searchTerm, snippetCount);
		}
		static void SearchIntentClient(string searchTerm, int snippetCount = 10)
		{
			var result = new SearchIntentClient(true).Get(searchTerm, snippetCount);
		}
		static void OnPageClient(string url, int maxUrlsCount)
		{
            var result = new OnPageClient().Get(url, maxUrlsCount);

			var germanStopwordList = DBStopwords.GetGerman();
			var variableGermanStopwordList = DBStopwords.GetVariableGerman();

			var wordList = new Dictionary<string, int>();

            foreach (var item in result.HtmlPages)
            {
                var itemWordList = new TextContentAnalyzer().AnalyzeContentFromHtml(item.ContentAsString).GetWordList();

                foreach (var word in itemWordList)
                {
					if(!variableGermanStopwordList.Contains(word.Key))
					{
						if (wordList.ContainsKey(word.Key))
						{
							wordList[word.Key] = wordList[word.Key] + word.Value;
						}
						else
						{
							wordList.Add(word.Key, word.Value);
						}
					}
                }
			}

            wordList = wordList.OrderByDescending(x => x.Value).ToDictionary(x => x.Key, x => x.Value);

			var searchIntentResult = new SearchIntentClient(false).Get(wordList);
        }
		static void WappalizerClient(string url, int maxUrls = 10)
		{
			var client = new WappalyzerClient();
			var result = client.Analyze(url, maxUrls);
		}
		static void UpdateSearchIntentDomains()
		{
			var applicationNames = new List<string>();
			var client = new WappalyzerClient();
			var domains = DBSearchIntentDomains.Get(true);
			var domainsFiltered = domains.Where(x => x.Value == Models.Google.SearchIntentType.None).Take(100).ToDictionary(x => x.Key, y => y.Value);

			foreach (var domain in domainsFiltered)
			{
				var searchIntentType = SearchIntentType.None;
				var requestUrl = string.Format("https://{0}", domain.Key);

				logger.Debug("Request: " + requestUrl);
				var clientData = client.Analyze(requestUrl);

				logger.Debug(clientData.success);
				if(clientData.data != null)
				{
					if (clientData.data.applications.Count() > 0)
					{
						foreach (var application in clientData.data.applications.Select(x => x.name))
						{
							if (!applicationNames.Contains(application))
							{
								logger.Debug(application);
								applicationNames.Add(application);
							}
						}

						if (clientData.data.applications.Any(x => x.name.ToLower().Contains("shop")))
						{
							searchIntentType = SearchIntentType.Transactional;
						}
					}
				}

				DBSearchIntentDomains.Update(domain.Key, searchIntentType);
			}
		}
	}
}
