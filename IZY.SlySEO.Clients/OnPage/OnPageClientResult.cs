﻿using IZY.SlySEO.Models.Website;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IZY.SlySEO.Clients.OnPage
{
    public class OnPageClientResult
    {
        public string StartUrl { get; set; }
        public int MaxUrlsCount { get; set; }
        public int UrlsCount { get; set; }
        public List<HtmlPage> HtmlPages { get; set; }
    }
}
