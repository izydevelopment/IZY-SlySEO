﻿using IZY.SlySEO.Clients.Web;
using IZY.SlySEO.Models.Website;
using IZY.SlySEO.Utilities.Analyzers;
using IZY.SlySEO.Utilities.Logic;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace IZY.SlySEO.Clients.OnPage
{
	public class OnPageClient
	{
		private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		public bool IsProgessing { get; set; } = false;
		public List<string> UrlsToProgress { get; set; } = new List<string>();
        public List<string> ProgressedUrls { get; set; } = new List<string>();
        public int MaxUrlsCount { get; set; }
        public Uri StartUrl { get; set; }

        private List<HtmlPage> htmlPages { get; set; } = new List<HtmlPage>();

        public OnPageClientResult Get(string startUrl, int maxUrlsCount = 10, int taskCount = 5)
        {
			logger.Info("Starting Crawling on URL: " + startUrl);
            this.StartUrl = HelperMethods.TryCreateUri(startUrl);

            this.UrlsToProgress.Add(startUrl);
            this.MaxUrlsCount = maxUrlsCount;

            if(MaxUrlsCount <= 4)
            {
                taskCount = 1;
            }

            Task[] tasks = new Task[taskCount];

            for (int i = 0; i < taskCount; i++)
            {
				logger.Info("Starting Task: " + i);
                tasks[i] = new Task(() => CrawlUrls());
                tasks[i].Start();
				Thread.Sleep(3000);
            }

            Task.WaitAll(tasks);

            //CrawlUrls();

            var result = new OnPageClientResult()
            {
                StartUrl = startUrl,
                MaxUrlsCount = maxUrlsCount,
                UrlsCount = htmlPages.Count(),
                HtmlPages = htmlPages
            };

            return result;
		}

        private void CrawlUrls()
        {
			
            do
            {
                var url = this.UrlsToProgress.FirstOrDefault();
                this.UrlsToProgress.Remove(url);

				if(url != null)
				{
					logger.Debug("Progressing URL: " + url);
					var htmlPage = ProgressUrl(url);
					this.ProgressedUrls.Add(url);

					if (htmlPage.HtmlContentType == HtmlContentType.Webpage)
					{
						htmlPages.Add(htmlPage);

						foreach (var link in htmlPage.LinkedUrls)
						{
							if (!this.UrlsToProgress.Contains(link.Key) || !this.ProgressedUrls.Contains(link.Key) && HelperMethods.IsInternalUrl(this.StartUrl.Host, link.Key))
							{
								this.UrlsToProgress.Add(link.Key);
							}
						}
					}
				}

                       
            } while (this.UrlsToProgress.Count() > 0 && this.ProgressedUrls.Count() < this.MaxUrlsCount);

			logger.Info("Done Crawling");
        }

        private HtmlPage ProgressUrl(string url)
        {
            var webClient = new IzyWebClient();
            var webClientResult = webClient.Get(url);

            if(webClientResult.IsHttpStatus2xx3xx && webClientResult.ContentType == HtmlContentType.Webpage)
            {
                var htmlPage = new HtmlPageAnalyzer().AnalyzeWebsite(webClientResult.ResponseAsString, url);
                return htmlPage;
            }
            else
            {
                var htmlPage = new HtmlPage()
                {
                    HtmlContentType = HtmlContentType.Unknown
                };

                return htmlPage;
            }
        }
	}
}
