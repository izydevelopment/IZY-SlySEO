﻿using IZY.SlySEO.Clients.Web;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace IZY.SlySEO.Clients.Wappalyzer
{
	public class WappalyzerClient
	{
		private static string serverAddress = "195.201.42.220";
		private static int serverPort = 50933;
		private static string serverPassword = "c4woc8uu6f8y7crt";

		private IzyWebClient client;

		public WappalyzerClient()
		{
			this.client = new IzyWebClient();
			this.client.TimeoutInMilliseconds = 10000;
		}

		public WappalyzerClientAnalyseReuslt Analyze(string url, int maxUrls = 1)
		{
			var requestUrl = $"http://{serverAddress}:{serverPort}/?password={serverPassword}&url={HttpUtility.UrlEncode(url)}&maxUrls={maxUrls}";

			var response = this.client.Get(requestUrl);

			if (!String.IsNullOrEmpty(response.ResponseAsString))
			{
				var result = JsonConvert.DeserializeObject<WappalyzerClientAnalyseReuslt>(response.ResponseAsString);

				return result;
			}
			else
			{
				var result = new WappalyzerClientAnalyseReuslt()
				{
					success = false,
				};

				return result;
			}

		}

		public class WappalyzerClientAnalyseReuslt
		{
			public bool success { get; set; }
			public double version { get; set; }
			public WappalyzerClientAnalyseReuslt_Data data { get; set; }
		}
		public class WappalyzerClientAnalyseReuslt_Data
		{
			// public Urls urls { get; set; }
			public List<WappalyzerClientAnalyseReuslt_Application> applications { get; set; }
			public WappalyzerClientAnalyseReuslt_Meta meta { get; set; }
		}
		public class WappalyzerClientAnalyseReuslt_Meta
		{
			public string language { get; set; }
		}

		public class WappalyzerClientAnalyseReuslt_Application
		{
			public string name { get; set; }
			public string confidence { get; set; }
			public string version { get; set; }
			public string icon { get; set; }
			public string website { get; set; }

			[JsonProperty("categories")]
			public IEnumerable<IDictionary<int, string>> categoriesKeyValuePairs { get; set; }

			[JsonIgnore]
			public Dictionary<int, string> categories { get => this.categoriesKeyValuePairs.SelectMany(x => x).ToDictionary(x => x.Key, x => x.Value); }
		}
	}
}
