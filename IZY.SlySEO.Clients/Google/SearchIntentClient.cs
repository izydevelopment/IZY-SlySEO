﻿using IZY.SlySEO.Clients.Web;
using IZY.SlySEO.Data.DB;
using IZY.SlySEO.Models.Google;
using IZY.SlySEO.Utilities.Analyzers;
using IZY.SlySEO.Utilities.Logic;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IZY.SlySEO.Clients.Google
{
	public class SearchIntentClient
	{
		private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		private Dictionary<SearchIntentType, Dictionary<string, int>> SearchIntentTerms { get; set; }
		private Dictionary<string, SearchIntentType> SearchIntentDomains { get; set; }
		private List<SearchIntentType> SearchIntentTypes { get; set; } = new List<SearchIntentType>();
        public bool UseDatabase { get; set; }

        public SearchIntentClient(bool useDatabase = true)
		{
			this.SearchIntentTypes = GetSearchIntentTypes();
            this.UseDatabase = useDatabase;
			this.SearchIntentTerms = GetSearchIntentTerms();
			this.SearchIntentDomains = GetSearchIntentDomains();
		}

        private List<SearchIntentType> GetSearchIntentTypes()
		{
			return (new List<SearchIntentType>()
			{
				SearchIntentType.Informational,
				SearchIntentType.Navigational,
				SearchIntentType.Transactional
			});
		}
        private Dictionary<SearchIntentType, double> GetNewSearchIntentScores()
        {
            return new Dictionary<SearchIntentType, double>
            {
                { SearchIntentType.Navigational, 0 },
                { SearchIntentType.Transactional, 0 },
                { SearchIntentType.Informational, 0 }
            };
        }
		private Dictionary<SearchIntentType, Dictionary<string, int>> GetSearchIntentTerms()
		{
			var result = new Dictionary<SearchIntentType, Dictionary<string, int>>();

			foreach (var searchIntent in this.SearchIntentTypes)
			{
				result.Add(searchIntent, DBSearchIntentTerms.Get(searchIntent, this.UseDatabase));
			}

			return result;
		}
		private Dictionary<string, SearchIntentType> GetSearchIntentDomains()
		{
			return DBSearchIntentDomains.Get(this.UseDatabase);
		}

		public SearchIntentResult Get(string searchTerm, int snippetCount = 10)
		{
			var result = CategorizeGoogleSerp(searchTerm, snippetCount);

            if (this.UseDatabase && result.AnalyzedUrlsCount == snippetCount)
            {
                DBSearchIntentCrawlings.Add(result);
            }

            return result;
		}
		public SearchIntentResult Get(string url)
		{
			var result = CategorizeUrl(url);

			if (this.UseDatabase)
			{
				DBSearchIntentCrawlings.Add(result);
			}

			return result;
		}
		public SearchIntentResult Get(Dictionary<string, int> wordList)
		{
            var result = CategorizeWordList(wordList);

            if (this.UseDatabase)
            {
                DBSearchIntentCrawlings.Add(result);
            }

			return result;
		}

        private SearchIntentResult CategorizeGoogleSerp(string searchTerm, int snippetCount = 10)
        {
            var serpClient = new SerpClient();

            searchTerm = searchTerm.ToLower();
            var serp = serpClient.Get(searchTerm, snippetCount);

            var result = new SearchIntentResult()
            {
                SearchTerm = serp.SearchTerm,
                AnalyzedUrlsCount = serp.Snippets.Count()
            };

			if (!serp.IsContentLoaded)
			{
				return result;
			}

			logger.Info("Categorize Search Intent with: " + serp.SearchTerm);

			var scores = new List<Dictionary<SearchIntentType, double>>();

            #region Serp Models
            foreach (var snippet in serp.Snippets)
            {
                var snippetScores = CheckSnippetForSearchIntents(snippet);

                result.AnalyzedUrls.Add(new SearchIntentItem()
                {
                    Position = snippet.Position,
                    Url = snippet.Url.AbsoluteUri,
                    InformationalScore = snippetScores[SearchIntentType.Informational],
                    NavigationalScore = snippetScores[SearchIntentType.Navigational],
                    TransactionalScore = snippetScores[SearchIntentType.Transactional],
                    Winner = snippetScores.OrderByDescending(x => x.Value).FirstOrDefault().Key
                });

                scores.Add(snippetScores);
            }
            #endregion

            foreach (var score in scores)
            {
                foreach (var searchIntent in this.SearchIntentTypes)
                {
                    if (searchIntent == SearchIntentType.Informational)
                    {
                        result.InformationalScore += score[searchIntent];
                    }
                    if (searchIntent == SearchIntentType.Navigational)
                    {
                        result.NavigationalScore += score[searchIntent];
                    }
                    if (searchIntent == SearchIntentType.Transactional)
                    {
                        result.TransactionalScore += score[searchIntent];
                    }
                }
            }

			result = TransferScores(result);

            return result;
        }
        private Dictionary<SearchIntentType, double> CheckSnippetForSearchIntents(Snippet snippet)
        {
            var htmlPage = new HtmlPageAnalyzer().AnalyzeWebsite(snippet.ContentAsString, snippet.UrlAsString);
            var wordList = new TextContentAnalyzer().AnalyzeContentFromHtml(htmlPage.ContentAsString).GetWordList();

            var scores = GetNewSearchIntentScores();

            logger.Info("Checking: " + snippet.Url);


            foreach (var checkList in this.SearchIntentTerms)
            {
				scores[checkList.Key] += CheckDomainForSearchIntent(snippet.Url, checkList.Key, snippet.Position);
				scores[checkList.Key] += CheckTitleForSearchIntent(htmlPage.Title, checkList.Value, snippet.Position);
                scores[checkList.Key] += CheckUrlTextForSearchIntent(snippet.Url, checkList.Value, snippet.Position);
                scores[checkList.Key] += CheckWordsForSearchIntent(wordList, checkList.Value, snippet.Position);
                scores[checkList.Key] += CheckLinkedDomainsForSearchIntent(htmlPage.LinkedDomains, checkList.Value, snippet.Position);
            }

            return scores;
        }
		private SearchIntentResult CategorizeUrl(string url)
		{
			var result = new SearchIntentResult()
			{
				AnalyzedUrlsCount = 1
			};

			var scores = GetNewSearchIntentScores();

			var webResult = new IzyWebClient().Get(url);
			var htmlPage = new HtmlPageAnalyzer().AnalyzeWebsite(webResult.ResponseAsString, url);
			var wordList = new TextContentAnalyzer().AnalyzeContentFromHtml(webResult.ResponseAsString).GetWordList();

			foreach (var searchIntentType in this.SearchIntentTypes)
			{
				scores[searchIntentType] += CheckDomainForSearchIntent(new Uri(url), searchIntentType);
				scores[searchIntentType] += CheckTitleForSearchIntent(htmlPage.Title, this.SearchIntentTerms[searchIntentType]);
				scores[searchIntentType] += CheckUrlTextForSearchIntent(new Uri(url), this.SearchIntentTerms[searchIntentType]);
				scores[searchIntentType] += CheckWordsForSearchIntent(wordList, this.SearchIntentTerms[searchIntentType]);
				scores[searchIntentType] += CheckLinkedDomainsForSearchIntent(htmlPage.LinkedDomains, this.SearchIntentTerms[searchIntentType]);

				if (searchIntentType == SearchIntentType.Informational)
				{
					result.InformationalScore += scores[searchIntentType];
				}
				if (searchIntentType == SearchIntentType.Navigational)
				{
					result.NavigationalScore += scores[searchIntentType];
				}
				if (searchIntentType == SearchIntentType.Transactional)
				{
					result.TransactionalScore += scores[searchIntentType];
				}
			}

			result.AnalyzedUrls.Add(new SearchIntentItem()
			{
				Position = 0,
				Url = url,
				InformationalScore = scores[SearchIntentType.Informational],
				NavigationalScore = scores[SearchIntentType.Navigational],
				TransactionalScore = scores[SearchIntentType.Transactional],
				Winner = scores.OrderByDescending(x => x.Value).FirstOrDefault().Key
			});

			result.AnalyzedWordsCount = wordList.Count();
			result = TransferScores(result);

			return result;
		}
        private SearchIntentResult CategorizeWordList(Dictionary<string, int> wordList)
		{
			var result = new SearchIntentResult()
			{
				AnalyzedUrlsCount = 0,
                AnalyzedWordsCount = wordList.Count
			};

            var scores = GetNewSearchIntentScores();

            foreach (var searchIntentType in this.SearchIntentTypes)
            {
                scores[searchIntentType] += CheckWordsForSearchIntent(wordList, this.SearchIntentTerms[searchIntentType]);

                if (searchIntentType == SearchIntentType.Informational)
                {
                    result.InformationalScore += scores[searchIntentType];
                }
                if (searchIntentType == SearchIntentType.Navigational)
                {
                    result.NavigationalScore += scores[searchIntentType];
                }
                if (searchIntentType == SearchIntentType.Transactional)
                {
                    result.TransactionalScore += scores[searchIntentType];
                }
            }

			result = TransferScores(result);

            return result;
		}	

		private double CheckDomainForSearchIntent(Uri url, SearchIntentType searchIntentType, int position = 1)
		{
			double score = 0;

			var seachIntentDomainsFiltered = this.SearchIntentDomains.Where(x => x.Value == searchIntentType);

			foreach (var searchIntentDomain in seachIntentDomainsFiltered)
			{
				if (url.AbsoluteUri.Contains(searchIntentDomain.Key))
				{
					logger.Debug("Domain: " + url.AbsoluteUri);
					score += CalculateScoreWeighting(SearchIntentClientConfig.DomainScoreFactor, position);
				}
			}

			return score;
		}
		private double CheckTitleForSearchIntent(string content, Dictionary<string, int> checkList, int position = 1)
		{
			double score = 0;

			if (String.IsNullOrEmpty(content))
			{
				return score;
			}

			var words = content.Split(' ');

			foreach (var word in words)
			{
				if (checkList.ContainsKey(word))
				{
					logger.Debug(String.Format("Title: {0}",  word));
					score += CalculateScoreWeighting(checkList[word] * SearchIntentClientConfig.TitleScoreFactor, position = 1);
				}
				if (score > SearchIntentClientConfig.TitleScoreCap)
				{
					logger.Warn("Title Limit Reached");
					return SearchIntentClientConfig.TitleScoreCap;
				}
			}

			return score;
		}
		private double CheckUrlTextForSearchIntent(Uri url, Dictionary<string, int> checkList, int position = 1)
		{
			double score = 0;

			foreach (var item in checkList)
			{
				if (url.AbsoluteUri.Contains(item.Key))
				{
					logger.Debug("URL: " + url.AbsoluteUri);
					score += CalculateScoreWeighting(checkList[item.Key] * SearchIntentClientConfig.UrlScoreFactor / ((double)checkList.Count / 100), position);
				}
			}

			return score;
		}
		private double CheckWordsForSearchIntent(Dictionary<string, int> words, Dictionary<string, int> checkList, int position = 1)
		{
			double score = 0;

			foreach (var word in words)
			{
				if (checkList.ContainsKey(word.Key))
				{
					logger.Debug(String.Format("Word: {0} x {1}", word.Value, word.Key));
					double wordScoreFactor = checkList[word.Key];

					score += CalculateScoreWeighting((((wordScoreFactor * SearchIntentClientConfig.WordScoreFactor) / checkList.Count()) * word.Value), position);
				}
				if (score >= SearchIntentClientConfig.WordScoreCap)
				{
					logger.Warn("Word Limit Reached");
					return SearchIntentClientConfig.WordScoreCap;
				}
			}

			return Math.Round(score, 2);
		}
		private double CheckLinkedDomainsForSearchIntent(Dictionary<string, int> links, Dictionary<string, int> checkList, int position = 1)
		{
			double score = 0;

			foreach (var link in links)
			{
				if (checkList.ContainsKey(link.Key))
				{
					logger.Debug(String.Format("Link: {0} x {1}", link.Value, link.Key));
					score += CalculateScoreWeighting(((checkList[link.Key] * SearchIntentClientConfig.LinkedDomainScoreFactor) / ((double)checkList.Count / 100) * link.Value), position);
				}
				if (score >= SearchIntentClientConfig.LinkedDomainScoreCap)
				{
					logger.Warn("Link Limit Reached");
					return SearchIntentClientConfig.LinkedDomainScoreCap;
				}
			}

			return score;
		}
		private SearchIntentResult TransferScores(SearchIntentResult searchIntentResult)
		{
			var sum = searchIntentResult.InformationalScore + searchIntentResult.NavigationalScore + searchIntentResult.TransactionalScore;

			if (searchIntentResult.InformationalScore > searchIntentResult.NavigationalScore && searchIntentResult.InformationalScore > searchIntentResult.TransactionalScore)
			{
				searchIntentResult.Winner = SearchIntentType.Informational;
				searchIntentResult.ProbabilityPercentage = Math.Round((searchIntentResult.InformationalScore / sum) * 100, 2);
			}
			if (searchIntentResult.NavigationalScore > searchIntentResult.InformationalScore && searchIntentResult.NavigationalScore > searchIntentResult.TransactionalScore)
			{
				searchIntentResult.Winner = SearchIntentType.Navigational;
				searchIntentResult.ProbabilityPercentage = Math.Round((searchIntentResult.NavigationalScore / sum) * 100, 2);
			}
			if (searchIntentResult.TransactionalScore > searchIntentResult.InformationalScore && searchIntentResult.TransactionalScore > searchIntentResult.NavigationalScore)
			{
				searchIntentResult.Winner = SearchIntentType.Transactional;
				searchIntentResult.ProbabilityPercentage = Math.Round((searchIntentResult.TransactionalScore / sum) * 100, 2);
			}

			searchIntentResult.InformationalScore = Math.Round(searchIntentResult.InformationalScore, 2);
			searchIntentResult.NavigationalScore = Math.Round(searchIntentResult.NavigationalScore, 2);
			searchIntentResult.TransactionalScore = Math.Round(searchIntentResult.TransactionalScore, 2);

			logger.Info("Informational: " + searchIntentResult.InformationalScore + " Navigational: " + searchIntentResult.NavigationalScore + " Transactional: " + searchIntentResult.TransactionalScore);

			return searchIntentResult;
		}
		private double CalculateScoreWeighting(double score, int position)
		{
			var result = score / (1 + ((double)position / SearchIntentClientConfig.PositionScoreFactor));

			return Math.Round(result, 2, MidpointRounding.AwayFromZero);
		}
	}
}