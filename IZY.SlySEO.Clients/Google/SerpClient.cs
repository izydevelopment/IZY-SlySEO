﻿using HtmlAgilityPack;
using IZY.SlySEO.Clients.Web;
using IZY.SlySEO.Data.DB;
using IZY.SlySEO.Models.Google;
using IZY.SlySEO.Utilities.Analyzers;
using log4net;
using System;
using System.Linq;

namespace IZY.SlySEO.Clients.Google
{
    public class SerpClient
    {
		private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
		private bool UseDatabase { get; set; }

		public SerpClient(bool useDatabase = true)
		{
			this.UseDatabase = useDatabase;
		}

		/// <summary>
		/// Analysiert die Suchergebnisse der Google SERP und liefert ein Ergebniss mit den rankenden Seiten
		/// </summary>
		/// <param name="searchTerm">Der Suchbegriff mithilfe die zugehörige SERP ermittelt werden soll</param>
		/// <param name="snippetCount">Die Anzahl der Suchergebnisse die berücksichtigt werden sollen</param>
		/// <returns></returns>
		public GoogleSerp Get(string searchTerm, int snippetCount = 20)
        {
            var googleSearchQuery = BuildGoogleSearchQuery(searchTerm, snippetCount);
            var contentAsString = MakeGoogleRequest(googleSearchQuery);
            var result = ParseGoogleRequestResult(contentAsString, searchTerm, snippetCount);

			if (this.UseDatabase)
			{
				DBSerpCrawlings.Add(result);
			}

           

            return result;
        }
        private string BuildGoogleSearchQuery(string searchTerm, int snippetCount)
        {
            searchTerm = searchTerm.Replace(" ", "+");

			if(snippetCount < 100)
			{
				snippetCount = 100;
			}
            return String.Format("http://www.google.com/search?q={0}&num={1}", searchTerm, snippetCount);
        }
        private string MakeGoogleRequest(string googleSearchQuery)
        {
            var webClient = new IzyWebClient();
			logger.Info("Google Request with URL: " + googleSearchQuery);
            var result = webClient.Get(googleSearchQuery);

			if (!result.IsHttpStatus2xx3xx)
			{
				logger.Warn("Error During Request StatusCode: " + result.HttpStatusCode);
			}
			if(result.HttpStatusCode == 429)
			{
				logger.Error("Blocked To Many Requests");
			}

            return result.ResponseAsString;
        }
        private GoogleSerp ParseGoogleRequestResult(string contentAsString, string searchTerm, int snippetCount = 20)
        {
			var doc = new HtmlDocument();
            var result = new GoogleSerp()
			{
				SearchTerm = searchTerm
			};

			if (String.IsNullOrEmpty(contentAsString))
			{
				result.IsContentLoaded = false;
			}
			else
			{
				doc.LoadHtml(contentAsString);
				result.IsContentLoaded = true;

				var node = doc.DocumentNode.SelectNodes("//div[@class='g']//div[@class='r']//a");
				var urls = node.Select(x => x.Attributes["href"].Value);

				if (urls != null)
				{
					foreach (var url in urls)
					{
						if (result.Snippets.Count >= snippetCount)
						{
							return result;
						}
						if (url == "#" || url.Contains("webcache.googleusercontent.com") || url.Contains("/search?q=related:"))
						{
							continue;
						}

						var webClient = new IzyWebClient();
						var webClientResult = webClient.Get(url);

						var serpModel = new Snippet(searchTerm, url)
						{
							ContentAsString = webClientResult.ResponseAsString.Trim()
						};

						logger.Debug(url);
						result.Snippets.Add(serpModel);
					}
				}
			}

            return result;
        }
    }
}
