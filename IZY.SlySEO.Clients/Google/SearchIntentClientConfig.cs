﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IZY.SlySEO.Clients.Google
{
	public static class SearchIntentClientConfig
	{
		#region Factors
		public const int PositionScoreFactor = 100;
		public const int SearchTermScoreFactor = 10;
		public const int TitleScoreFactor = 5;
		public const int UrlScoreFactor = 5;
		public const int DomainScoreFactor = 20;
		public const int WordScoreFactor = 1;
		public const int LinkedDomainScoreFactor = 10;
		#endregion

		#region Caps
		public const int SearchTermScoreCap = 100;
		public const int TitleScoreCap = 20;
		public const int WordScoreCap = 100;
		public const int LinkedDomainScoreCap = 100;
		#endregion
	}
}
