﻿using IZY.SlySEO.Models.Website;
using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace IZY.SlySEO.Clients.Web
{
	public class IzyWebClient
	{
		#region Logging
		private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
		#endregion

		public static string DefaultUserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36";

		private bool acceptCookies;
		private bool allowAutoRedirect;

		public List<HtmlContentType> ContentTypesToAvoidDownload { get; private set; }
		public double BytesPerSecondTransferRate { get; set; }

		public int TimeoutInMilliseconds { get; set; }

		private List<HttpClientHandler> clientHandlers;
		private HttpClientHandler directClientHandler;
		private string defaultUserAgent;


		public IzyWebClientResult Get(string url)
		{
			return Get(url, this.defaultUserAgent);
		}
		public List<IzyWebClientResult> Get(List<string> urls, int maxRequests = 1)
		{
			var task = Task.Run(() => { return GetAsync(urls, maxRequests); });
			return task.GetAwaiter().GetResult();
		}
		public Task<List<IzyWebClientResult>> GetAsync(List<string> urls, int maxRequests)
		{
			return GetAsync(urls, maxRequests, this.defaultUserAgent);
		}
		public async Task<IzyWebClientResult> GetAsync(string url, string userAgent, bool retry = false, bool forceAvoidDownload = false)
		{
			return await Task<IzyWebClientResult>.Factory.StartNew(new Func<IzyWebClientResult>(() => { return Get(url, userAgent, retry, forceAvoidDownload); }));
		}


		public IzyWebClient(bool allowAutoRedirect = true, bool acceptCookies = true)
		{
			this.TimeoutInMilliseconds = 10000;

			this.defaultUserAgent = DefaultUserAgent;

			this.clientHandlers = new List<HttpClientHandler>();

			this.acceptCookies = acceptCookies;
			this.allowAutoRedirect = allowAutoRedirect;

			this.directClientHandler = new HttpClientHandler();
			CookieContainer cookies = new CookieContainer();
			this.directClientHandler.CookieContainer = cookies;
			this.directClientHandler.UseCookies = this.acceptCookies;
			this.directClientHandler.AllowAutoRedirect = this.allowAutoRedirect;
			this.directClientHandler.AutomaticDecompression = DecompressionMethods.None;

			this.clientHandlers.Add(this.directClientHandler);

			this.ContentTypesToAvoidDownload = new List<HtmlContentType>();
			this.BytesPerSecondTransferRate = 2000.0;

			ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback((s, c, d, e) => { return true; });
			ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
			ServicePointManager.DefaultConnectionLimit = 1000;
		}
		public async Task<List<IzyWebClientResult>> GetAsync(List<string> urls, int maxRequests, string userAgent, bool forceAvoidDownload = false)
		{
			int nextIndex = 0;
			var urlTasks = new List<Task<IzyWebClientResult>>();

			var result = new List<IzyWebClientResult>();

			while (nextIndex < maxRequests && nextIndex < urls.Count)
			{
				urlTasks.Add(GetAsync(urls[nextIndex], userAgent, false, forceAvoidDownload));
				nextIndex++;
			}

			while (urlTasks.Count > 0)
			{
				Task<IzyWebClientResult> urlTask = await Task.WhenAny(urlTasks);
				urlTasks.Remove(urlTask);

				var singleResult = await urlTask;
				result.Add(singleResult);

				if (nextIndex < urls.Count)
				{
					urlTasks.Add(GetAsync(urls[nextIndex], userAgent, false, forceAvoidDownload));
					nextIndex++;
				}
			}

			return result;
		}
		public IzyWebClientResult Get(string url, string userAgent, bool retry = false, bool forceAvoidDownload = false)
		{
			var result = new IzyWebClientResult(url);

			HttpResponseMessage response = null;

			HttpClientHandler handler;
			if (retry || !this.clientHandlers.Any())
			{
				handler = this.directClientHandler;
			}
			else
			{
				handler = this.clientHandlers.FirstOrDefault();
			}


			HttpClient client = new HttpClient(handler);

			var timer = Stopwatch.StartNew();


			client.DefaultRequestHeaders.Add("User-Agent", userAgent);
			client.DefaultRequestHeaders.Add("Accept-Charset", "utf-8");
			client.DefaultRequestHeaders.Add("Accept-Encoding", "gzip, deflate");

			client.Timeout = TimeSpan.FromMilliseconds(this.TimeoutInMilliseconds);

			result.ContentType = ParseContentTypeFromUrl(url);

			var isContentLoaded = false;


			try
			{
				var time = DateTime.Now;

				response = client.GetAsync(url, HttpCompletionOption.ResponseHeadersRead).GetAwaiter().GetResult();


				result.TimeToFirstByteInMilliseconds = (int)(DateTime.Now - time).TotalMilliseconds;

				result.HttpStatusCode = (int)response.StatusCode;

				result.ResponseUrl = string.IsNullOrWhiteSpace(response.RequestMessage.RequestUri.ToString()) ? result.ResponseUrl : response.RequestMessage.RequestUri.ToString();

				if (result.HttpStatusCode > 300 && result.HttpStatusCode < 309)
				{
					if (response.Headers.Location != null)
					{
						result.HeaderRedirectLocation = response.Headers.Location.ToString();
					}
					else
					{
						result.HeaderRedirectLocation = "";
					}

					result.IsHeaderRedirect = true;
				}

				result.ContentLanguage = response.Content.Headers.ContentLanguage.FirstOrDefault();

				result.ContentLength = response.Content.Headers.ContentLength.HasValue ? response.Content.Headers.ContentLength.Value : 0;
				result.ContentLengthDecompressed = result.ContentLength;

				result.ContentEncoding = response.Content.Headers.ContentEncoding.FirstOrDefault();

				var responseContentType = response.Content.Headers.ContentType == null ? "" : response.Content.Headers.ContentType.ToString().ToLower();
				result.ContentTypeValue = responseContentType;

				var contentDispositionValue = response.Content.Headers.Where(x => x.Key == "Content-Disposition").Select(x => x.Value).FirstOrDefault();
				if (contentDispositionValue != null && contentDispositionValue.Any(x => x.Contains("attachment")))
				{
					result.ContentType = HtmlContentType.Download;
				}

				if (!(result.HttpStatusCode >= 400 && result.HttpStatusCode < 600))
				{
					var mimeContentType = ParseContentTypeByMime(responseContentType);
					result.ContentType = mimeContentType != HtmlContentType.Unknown ? mimeContentType : result.ContentType;
				}
				else
				{
					if (result.HttpStatusCode == 403)
					{


					}
				}

				result.LoadingTimeInMilliseconds = (int)((double)result.ContentLength / (double)this.BytesPerSecondTransferRate);


				if (!this.ContentTypesToAvoidDownload.Contains(result.ContentType) && !forceAvoidDownload)
				{
					time = DateTime.Now;

					var task = response.Content.ReadAsByteArrayAsync();
					var taskWaiter = task.Wait(this.TimeoutInMilliseconds);

					if (taskWaiter)
					{
						result.ResponseAsBytes = task.GetAwaiter().GetResult();

						result.LoadingTimeInMilliseconds = (int)(DateTime.Now - time).TotalMilliseconds;

						result.ResponseAsBytesDecompressed = result.ResponseAsBytes;
						result.ContentLength = result.ResponseAsBytes.Length;
						result.ContentLengthDecompressed = result.ContentLength;

						isContentLoaded = true;
					}
					else
					{
					}
				}
				else
				{
					response.Content.Dispose();
				}

			}
			catch (HttpRequestException ex)
			{

				logger.Warn("HttpRequestException: " + url);
				logger.Warn(ex);

				if (!retry)
				{
					return Get(url, userAgent, true, forceAvoidDownload: forceAvoidDownload);
				}

				return result;
			}
			catch (AggregateException errors)
			{
				errors.Handle(e => e is TaskCanceledException);
			}
			catch (TaskCanceledException error)
			{
				logger.Warn("TaskCanceledException: " + url);
				logger.Warn(error);

				if (!retry)
				{
					return Get(url, userAgent, true);
				}

				return result;
			}
			catch (UriFormatException error)
			{
				logger.Warn("UriFormatException: " + url);
				logger.Warn(error);
				return result;
			}
			catch (Exception error)
			{
				logger.Warn("Exception: " + url);
				logger.Warn(error);
				return result;
			}

			if (isContentLoaded)
			{
				try
				{

					if (result.ContentEncoding == "gzip" && isContentLoaded)
					{
						using (GZipStream stream = new GZipStream(new MemoryStream(result.ResponseAsBytesDecompressed), CompressionMode.Decompress))
						{
							const int size = 4096;
							byte[] buffer = new byte[size];
							using (MemoryStream memory = new MemoryStream())
							{
								int count = 0;
								do
								{
									count = stream.Read(buffer, 0, size);
									if (count > 0)
									{
										memory.Write(buffer, 0, count);
									}
								}
								while (count > 0);
								result.ResponseAsBytesDecompressed = memory.ToArray();
								result.ContentLengthDecompressed = result.ResponseAsBytesDecompressed.Length;
							}
						}
					}

					if (result.ContentEncoding == "deflate" && isContentLoaded)
					{
						using (DeflateStream stream = new DeflateStream(new MemoryStream(result.ResponseAsBytesDecompressed), CompressionMode.Decompress))
						{
							const int size = 4096;
							byte[] buffer = new byte[size];
							using (MemoryStream memory = new MemoryStream())
							{
								int count = 0;
								do
								{
									count = stream.Read(buffer, 0, size);
									if (count > 0)
									{
										memory.Write(buffer, 0, count);
									}
								}
								while (count > 0);
								result.ResponseAsBytesDecompressed = memory.ToArray();
								result.ContentLengthDecompressed = result.ResponseAsBytesDecompressed.Length;
							}
						}
					}

					var encoding = Encoding.UTF8;

					if (result.HttpStatusCode == 200)
					{
						if (result.ContentType == HtmlContentType.Webpage
							|| result.ContentType == HtmlContentType.PlainText
							|| result.ContentType == HtmlContentType.Javascript
							|| result.ContentType == HtmlContentType.Css)
						{
							if (result.ContentTypeValue.Contains("utf-8"))
							{
								encoding = Encoding.UTF8;
							}
							else if (result.ContentTypeValue.Contains("iso-8859-15"))
							{
								encoding = Encoding.GetEncoding("iso-8859-15");
							}
							else if (result.ContentTypeValue.Contains("iso-8859-1"))
							{
								encoding = Encoding.GetEncoding("iso-8859-1");
							}
							else if (result.ContentTypeValue.Contains("windows-1252"))
							{
								encoding = Encoding.GetEncoding(1252);
							}
							else if (result.ContentTypeValue.Contains("charset"))
							{
								logger.WarnFormat("Unsupportet content-encoding: {0}", result.ContentTypeValue);
							}

							result.ResponseAsString = encoding.GetString(result.ResponseAsBytesDecompressed);
						}
					}
				}
				catch (Exception ex)
				{
					logger.Error("Exception: ", ex);
				}
			}

			timer.Reset();

			return result;
		}


		private HtmlContentType ParseContentTypeFromUrl(string url)
		{
			try
			{
				Uri uri = new Uri(url.ToLower());

				var contentTypes = new Dictionary<List<string>, HtmlContentType>();
				contentTypes.Add(new List<string> { "js" }, HtmlContentType.Javascript);
				contentTypes.Add(new List<string> { "css" }, HtmlContentType.Css);
				contentTypes.Add(new List<string> { "bmp", "gif", "jpg", "jpeg", "png" }, HtmlContentType.Image);
				contentTypes.Add(new List<string> { "avi", "mpg", "mpeg", "mov", "flv" }, HtmlContentType.Video);
				contentTypes.Add(new List<string> { "zip", "exe", "pdf", "mov", "csv" }, HtmlContentType.Download);
				contentTypes.Add(new List<string> { "html", "htm", "php", "php5", "asax" }, HtmlContentType.Webpage);
				contentTypes.Add(new List<string> { "mp3", "wav" }, HtmlContentType.Audio);

				string localPath = uri.LocalPath.ToLower();

				foreach (var item in contentTypes)
				{
					foreach (var iitem in item.Key)
					{
						if (localPath.EndsWith("." + iitem))
						{
							return item.Value;
						}
					}
				}

				if (!localPath.Contains("."))
				{
					return HtmlContentType.Webpage;
				}
			}
			catch (Exception ex)
			{
				logger.Error(string.Format("Error during ParseContentTypeFromUrl from uri {0}.", url), ex);
			}

			return HtmlContentType.Unknown;
		}

		private HtmlContentType ParseContentTypeByMime(string value)
		{
			if (value.Contains("text/html"))
			{
				return HtmlContentType.Webpage;
			}

			if (value.Contains("text/plain"))
			{
				return HtmlContentType.PlainText;
			}

			if (value.ToLower().Contains("image/"))
			{
				return HtmlContentType.Image;
			}


			if (value.ToLower().Contains("video/"))
			{
				return HtmlContentType.Video;
			}

			if (value.ToLower().Contains("audio/"))
			{
				return HtmlContentType.Audio;
			}

			if (value.ToLower().Contains("text/css"))
			{
				return HtmlContentType.Css;
			}

			if (value.ToLower().Contains("text/xml"))
			{
				return HtmlContentType.Xml;
			}

			if (value.ToLower().Contains("text/javascript") || value.ToLower().Contains("application/javascript") || value.ToLower().Contains("application/x-javascript"))
			{
				return HtmlContentType.Javascript;
			}

			if (value.ToLower().Contains("application/zip") || value.ToLower().Contains("application/pdf") || value.ToLower().Contains("text/csv") || value.ToLower().Contains("application/x-msdownload")
				|| value.ToLower().Contains("application/octet-stream") || value.ToLower().Contains("application/x-compressed"))
			{
				return HtmlContentType.Download;
			}

			return HtmlContentType.Unknown;
		}

	}
}
