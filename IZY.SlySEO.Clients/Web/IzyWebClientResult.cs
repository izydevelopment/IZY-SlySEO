﻿using IZY.SlySEO.Models.Website;

namespace IZY.SlySEO.Clients.Web
{
	public class IzyWebClientResult
	{
		public string UrlAsRequested { get; set; }
		public string ResponseUrl { get; set; }
		public long ContentLength { get; set; }
		public long ContentLengthDecompressed { get; set; }
		public byte[] ResponseAsBytes { get; set; }
		public byte[] ResponseAsBytesDecompressed { get; set; }
		public string ResponseAsString { get; set; }
		public int? TimeToFirstByteInMilliseconds { get; set; }
		public int? LoadingTimeInMilliseconds { get; set; }
		public int HttpStatusCode { get; set; }
		public bool IsHttpStatus2xx3xx { get { return this.HttpStatusCode >= 200 && this.HttpStatusCode < 400; } }
		public bool IsHttpStatus2xx { get { return this.HttpStatusCode >= 200 && this.HttpStatusCode < 300; } }
		public string ContentTypeValue { get; set; }
		public HtmlContentType ContentType { get; set; }
		public string ContentEncoding { get; set; }
		public string ContentLanguage { get; set; }
		public bool IsHeaderRedirect { get; set; }
		public string HeaderRedirectLocation { get; set; }

		public IzyWebClientResult(string urlAsRequested)
		{
			this.ContentType = HtmlContentType.Unknown;
			this.UrlAsRequested = urlAsRequested;
			this.ResponseUrl = urlAsRequested;

			this.IsHeaderRedirect = false;
			this.ResponseAsString = "";
		}

	}
}
