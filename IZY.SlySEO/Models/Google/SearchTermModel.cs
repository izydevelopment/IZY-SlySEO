﻿using System;

namespace IZY.SlySEO.Models.Google
{
	public class SearchTermModel
	{
		public string Term { get; set; }
		public int? ScoreMultiplier { get; set; }
		public SearchIntentType SearchIntentType { get; set; }
		public DateTime DateTime { get; set; }
	}
}
