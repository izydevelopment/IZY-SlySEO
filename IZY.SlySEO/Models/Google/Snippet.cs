﻿using System;

namespace IZY.SlySEO.Models.Google
{
	public class Snippet
    {
        private static int Count { get; set; } = 1;
        public string SearchTerm { get; set; }
        public int Position { get; set; }
        public string UrlAsString { get; set; }
        public Uri Url { get; set; }
        public string ContentAsString { get; set; }

		public Snippet(string searchTerm, string url)
        {
            this.SearchTerm = searchTerm;
            this.Position = Count++;
            this.UrlAsString = url;
            this.Url = new Uri(url);
        }
    }
}
