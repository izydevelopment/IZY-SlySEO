﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IZY.SlySEO.Models.Google
{
	public class SearchIntentItem
	{
		public int Position { get; set; }
		public string Url { get; set; }
		public double InformationalScore { get; set; }
		public double TransactionalScore { get; set; }
		public double NavigationalScore { get; set; }
		public SearchIntentType Winner { get; set; }
	}
}
