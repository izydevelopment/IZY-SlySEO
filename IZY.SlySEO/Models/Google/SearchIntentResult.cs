﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IZY.SlySEO.Models.Google
{
	public class SearchIntentResult
	{
		public string SearchTerm { get; set; }
		public int AnalyzedUrlsCount { get; set; }
        public int AnalyzedWordsCount { get; set; }
        public double InformationalScore { get; set; }
		public double NavigationalScore { get; set; }
		public double TransactionalScore { get; set; }
		public List<SearchIntentItem> AnalyzedUrls { get; set; } = new List<SearchIntentItem>();
		public double ProbabilityPercentage { get; set; }
		public SearchIntentType Winner { get; set; }
	}
}
