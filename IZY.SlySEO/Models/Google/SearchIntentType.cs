﻿namespace IZY.SlySEO.Models.Google
{
    public enum SearchIntentType
    {
		Informational = 1,
		Navigational = 2,
        Transactional = 3,    
		None = 4
    }
}
