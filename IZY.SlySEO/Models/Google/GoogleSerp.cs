﻿using System.Collections.Generic;

namespace IZY.SlySEO.Models.Google
{
    public class GoogleSerp
    {
        public string SearchTerm { get; set; }
        public List<Snippet> Snippets { get; set; } = new List<Snippet>();
		public bool IsContentLoaded { get; set; }
		public int Id { get; set; }
	}
}
