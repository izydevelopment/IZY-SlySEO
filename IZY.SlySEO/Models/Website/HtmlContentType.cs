﻿namespace IZY.SlySEO.Models.Website
{
	public enum HtmlContentType
	{
		Webpage = 1,
		Image = 2,
		Download = 3,
		Javascript = 4,
		Css = 5,
		Video = 6,
		PlainText = 7,
		Xml = 7,
		Audio = 8,

		Unknown = 99
	}
}
