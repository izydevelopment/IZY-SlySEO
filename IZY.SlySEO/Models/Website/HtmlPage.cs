﻿using HtmlAgilityPack;
using System.Collections.Generic;

namespace IZY.SlySEO.Models.Website
{
	public class HtmlPage
    {
        public HtmlContentType HtmlContentType { get; set; }
		public string ContentAsString { get; set; }
		public string Title { get; set; }
        public string MetaDescription { get; set; }
		public List<string> Scripts { get; set; }
		public Dictionary<int, List<string>> Headlines { get; set; }
        public Dictionary<string, int> LinkedDomains { get; set; }
        public Dictionary<string, int> LinkedUrls { get; set; }
        public string Text { get; set; }
	}
}
