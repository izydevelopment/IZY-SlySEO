﻿using log4net;
using log4net.Appender;
using log4net.Layout;
using log4net.Repository.Hierarchy;
using System.Net;
using System.Reflection;

namespace IZY.SlySEO.Utilities.Logging
{
	public class Log4ViewConfig
	{
		public static void ConfigureUdp()
		{
			Hierarchy hierarchy = (Hierarchy)LogManager.GetRepository(Assembly.GetCallingAssembly());

			XmlLayout layout = new XmlLayout();
			layout.LocationInfo = true;
			layout.ActivateOptions();


			UdpAppender udpAppender = new UdpAppender();
			udpAppender.RemoteAddress = IPAddress.Parse("127.0.0.1");
			udpAppender.RemotePort = 877;
			udpAppender.Layout = layout;
			udpAppender.ActivateOptions();


			hierarchy.Root.AddAppender(udpAppender);


			hierarchy.Root.Level = log4net.Core.Level.Debug;
			hierarchy.Configured = true;
		}
	}
}
