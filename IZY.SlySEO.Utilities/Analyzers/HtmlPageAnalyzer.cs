﻿using HtmlAgilityPack;
using IZY.SlySEO.Models.Website;
using IZY.SlySEO.Utilities.Logic;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IZY.SlySEO.Utilities.Analyzers
{
    public class HtmlPageAnalyzer : HtmlAnalyzer
    {
        private Uri Url { get; set; }

        public HtmlPage AnalyzeWebsite(string contentAsString, string url)
        {
            this.Url = HelperMethods.TryCreateUri(url);

            var doc = new HtmlDocument();
            doc.LoadHtml(contentAsString);

			#region Scripts
			var scripts = new List<string>();
			var scriptNodes = doc.DocumentNode.SelectNodes("//script[@src]");

			if(scriptNodes != null)
			{
				foreach (var scriptNode in scriptNodes)
				{
					var srcValue = scriptNode.Attributes["src"].Value;
					scripts.Add(srcValue);
				}
			}
			#endregion

			#region Title
			var titleNode = doc.DocumentNode.SelectSingleNode("//title");
            var title = titleNode?.InnerHtml;
            
            if (title != null)
            {
                title = HelperMethods.GetCleanContent(title, true);
            }
            #endregion

            #region Meta Description
            var metaDescriptionNode = doc.DocumentNode.SelectSingleNode("//meta[@name='description']");
            var metaDescription = metaDescriptionNode?.InnerHtml;

            if (metaDescription != null)
            {
                metaDescription = HelperMethods.GetCleanContent(metaDescription, true);
            }
            #endregion

            #region Text
            var text = new TextContentAnalyzer().AnalyzeContentFromHtml(contentAsString).GetTextContent();
			#endregion

			#region Headlines
			var headlines = GetHeadlines(contentAsString);
            #endregion

            #region Domains
            var linkNodes = doc.DocumentNode.SelectNodes("//a");

            var domains = new Dictionary<string, int>();

            if (linkNodes != null)
            {
                foreach (var item in linkNodes)
                {
                    var value = item.GetAttributeValue("href", "");

                    if (value.Contains("http") || value.Contains("https"))
                    {
                        var uri = HelperMethods.TryCreateUri(value);

                        if (uri == null)
                        {
                            continue;
                        }

                        if (!domains.Keys.Contains(uri.Host))
                        {
                            domains.Add(uri.Host, 1);
                        }
                        else
                        {
                            domains[uri.Host]++;
                        }


                    }
                }
            }
            #endregion

            #region Urls
            var urls = new Dictionary<string, int>();

            if (linkNodes != null)
            {
                foreach (var item in linkNodes)
                {
                    var value = item.GetAttributeValue("href", "");

                    if (value.Contains("http") || value.Contains("https"))
                    {
                        var uri = HelperMethods.TryCreateUri(value);
                        
                        if (uri == null)
                        {
                            continue;
                        }
                        if (uri.IsAbsoluteUri)
                        {
                            var absoluteUri = uri.AbsoluteUri;

                            if (!urls.Keys.Contains(absoluteUri))
                            {
                                urls.Add(absoluteUri, 1);
                            }
                            else
                            {
                                urls[absoluteUri]++;
                            }
                        }
                        else
                        {
                            var absoluteUrl = "https://" + this.Url.Host + uri.AbsolutePath;

                            if (!urls.Keys.Contains(absoluteUrl))
                            {
                                urls.Add(absoluteUrl, 1);
                            }
                            else
                            {
                                urls[absoluteUrl]++;
                            }
                        }
                    }
                }
            }
            #endregion

            var result = new HtmlPage()
            {
				ContentAsString = contentAsString,
                HtmlContentType = HtmlContentType.Webpage,
                Title = title,
                MetaDescription = metaDescription,
                Headlines = headlines,
                Text = text,
                LinkedDomains = domains,
                LinkedUrls = urls,
				Scripts = scripts
            };

            return result;
        }

        private Dictionary<int, List<string>> GetHeadlines(string contentAsString)
        {
            var result = new Dictionary<int, List<string>>();

            var h1Nodes = GetHtmlNodes(contentAsString, "//h1");

            if (h1Nodes != null)
            {
                result.Add(1, new List<string>());

                foreach (HtmlNode h1 in h1Nodes)
                {
                    string h1Value = h1.InnerHtml;

                    result.Where(x => x.Key == 1).FirstOrDefault().Value.Add(h1Value);
                }
            }

            var h2Nodes = GetHtmlNodes(contentAsString, "//h2");

            if (h2Nodes != null)
            {
                result.Add(2, new List<string>());

                foreach (HtmlNode h2 in h2Nodes)
                {
                    string h2Value = h2.InnerHtml;

                    result.Where(x => x.Key == 2).FirstOrDefault().Value.Add(h2Value);
                }
            }

            var h3Nodes = GetHtmlNodes(contentAsString, "//h3");

            if (h3Nodes != null)
            {
                result.Add(3, new List<string>());

                foreach (HtmlNode h3 in h3Nodes)
                {
                    string h3Value = h3.InnerHtml;


                    result.Where(x => x.Key == 3).FirstOrDefault().Value.Add(h3Value);
                }
            }

            return result;
        }
    }
}
