﻿using HtmlAgilityPack;
using IZY.SlySEO.Utilities.Logic;
using System;

namespace IZY.SlySEO.Utilities.Analyzers
{
	public class HtmlAnalyzer
    {
        public HtmlDocument HtmlDocument { get; set; } = new HtmlDocument();

        public HtmlNodeCollection GetHtmlNodes(string contentAsString, string xpath)
        {
            try
            {
                this.HtmlDocument.LoadHtml(contentAsString);
                var result = this.HtmlDocument.DocumentNode.SelectNodes(xpath);
                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }

		public HtmlNode GetHtmlNode(string contentAsString, string xpath)
		{
			try
			{
				this.HtmlDocument.LoadHtml(contentAsString);
				var result = this.HtmlDocument.DocumentNode.SelectSingleNode(xpath);
				return result;
			}
			catch (Exception)
			{
				return null;
			}
		}
	}
}
