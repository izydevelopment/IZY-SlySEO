﻿using HtmlAgilityPack;
using log4net;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace IZY.SlySEO.Utilities.Analyzers
{
	public class TextContentAnalyzer
	{
		private List<string> stopWords;
		public TextContentAnalyzer(List<string> stopWords)
		{
			this.stopWords = stopWords;
		}

		public TextContentAnalyzer() : this(new List<string>())
		{

		}


		public TextContentAnalyzerResult AnalyzeContent(List<string> textBlocks, bool setHyphenAsWordDivider = false)
		{
			var result = new TextContentAnalyzerResult(textBlocks, this.stopWords, setHyphenAsWordDivider);
			return result;
		}


		public TextContentAnalyzerResult AnalyzeContentFromHtml(HtmlDocument doc, int minTextBlockLenght = 40, bool setHyphenAsWordDivider = false)
		{
			var result = new TextContentAnalyzerResult(GetTextBlocks(doc, minTextBlockLenght), this.stopWords, setHyphenAsWordDivider);
			return result;
		}

		public TextContentAnalyzerResult AnalyzeContentFromHtml(string html, int minTextBlockLenght = 40, bool setHyphenAsWordDivider = false)
		{
			var doc = new HtmlDocument();
			doc.LoadHtml(html);
			return AnalyzeContentFromHtml(doc, minTextBlockLenght, setHyphenAsWordDivider);
		}

		public string GetReducedHtml(HtmlDocument doc, int minTextBlockLenght = 40)
		{
			var result = "";

			var rootNode = doc.DocumentNode;
			var bodyNode = rootNode.SelectSingleNode("//body");
			if (bodyNode != null)
			{
				rootNode = bodyNode;
			}

			// Remove tags
			rootNode.Descendants()
					.Where(n => n.Name == "script" || n.Name == "style" || n.NodeType == HtmlNodeType.Comment || n.Name == "head")
					.ToList()
					.ForEach(n => n.Remove());

			foreach (var eachNode in rootNode.Descendants().Where(x => x.NodeType == HtmlNodeType.Element))
			{
				eachNode.Attributes.RemoveAll();
			}

			foreach (var eachNode in rootNode.Descendants().Where(x => x.NodeType == HtmlNodeType.Element).ToList())
			{
				if (string.IsNullOrWhiteSpace(eachNode.InnerText))
				{
					eachNode.Remove();
				}
			}


			var innerHtml = rootNode.InnerHtml;


			// Remove the inline html tags
			innerHtml = Regex.Replace(innerHtml, "</?(b|strong|a|i|span)[^>]*>", "", RegexOptions.IgnoreCase);


			// Remove newlines and multi whitespace
			innerHtml = Regex.Replace(innerHtml, "[\t\r\n]", " ");
			innerHtml = Regex.Replace(innerHtml, "[ ]{2,}", " ");

			innerHtml = HttpUtility.HtmlDecode(innerHtml);


			// Find Textblocks
			var lastString = new StringBuilder();
			var lastTag = new StringBuilder();
			var insideTag = false;


			var allowedTags = new List<string> { "h1", "h2", "h3", "h4", "h5", "h6" };

			for (int i = 0; i < innerHtml.Length; i++)
			{
				var c = innerHtml[i];



				if (c == '<')
				{
					insideTag = true;


					if (lastString.Length > minTextBlockLenght && lastString.ToString().Trim().Length > minTextBlockLenght)
					{
						var tag = lastTag.ToString().ToLower().Trim();
						if (!allowedTags.Contains(tag))
						{
							tag = "p";
						}

						result += ($"<{tag}>{lastString.ToString()}</{tag}>");
					}
					lastString.Clear();
					lastTag.Clear();
				}
				else if (c == '>')
				{
					insideTag = false;
				}
				else if (!insideTag)
				{
					lastString.Append(c);
				}

				if (insideTag && c != '<')
				{
					lastTag.Append(c);
				}

			}


			return result;
		}



		public List<string> GetTextBlocks(HtmlDocument doc, int minTextBlockLenght = 40)
		{
			var result = new List<string>();

			result = new List<string>();

			var rootNode = doc.DocumentNode;
			var bodyNode = rootNode.SelectSingleNode("//body");
			if (bodyNode != null)
			{
				rootNode = bodyNode;
			}

			// Remove tags
			rootNode.Descendants()
					.Where(n => n.Name == "script" || n.Name == "style" || n.NodeType == HtmlNodeType.Comment || n.Name == "head")
					.ToList()
					.ForEach(n => n.Remove());

			// Attribute von HTML-Elementen entfernen, da diese HTML enthalten könne, was durch "HtmlDecode" zum Vorschein kommt
			foreach (var eachNode in rootNode.Descendants().Where(x => x.NodeType == HtmlNodeType.Element))
			{
				eachNode.Attributes.RemoveAll();
			}

			var innerHtml = rootNode.InnerHtml;

			// Remove the inline html tags
			innerHtml = Regex.Replace(innerHtml, "</?(b|strong|a|i|span)[^>]*>", "", RegexOptions.IgnoreCase);


			// Remove newlines and multi whitespace
			innerHtml = Regex.Replace(innerHtml, "[\t\r\n]", " ");
			innerHtml = Regex.Replace(innerHtml, "[ ]{2,}", " ");

			// Remove html-entities
			innerHtml = HttpUtility.HtmlDecode(innerHtml);

			// Remove shy-ascii-character
			innerHtml = innerHtml.Replace(((char)173).ToString(), "");

			// Find Textblocks
			var lastString = new StringBuilder();
			var insideTag = false;
			for (int i = 0; i < innerHtml.Length; i++)
			{
				var c = innerHtml[i];
				if (c == '<')
				{
					insideTag = true;

					if (lastString.Length > minTextBlockLenght && lastString.ToString().Trim().Length > minTextBlockLenght)
					{
						result.Add(lastString.ToString().Trim());
					}
					lastString.Clear();
				}
				else if (c == '>')
				{
					insideTag = false;
				}
				else if (!insideTag)
				{
					lastString.Append(c);
				}
			}

			return result;
		}	

	}

	public class TextContentAnalyzerResult
	{
		#region Logging
		private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
		#endregion


		private List<string> stopWords;

		private List<string> textBlocks;

		private string textContent;
		private string textContentOnlyWords;
		private string textContentOnlyWordsWithoutStopwords;

		private int? numberOfWords;
		private int? numberOfWordswithoutStopwords;
		private int? numberOfDifferentWords;

		private Dictionary<int, Dictionary<string, int>> wordLists;
		private Dictionary<int, Dictionary<string, int>> wordListsWithoutStopwords;
		private List<char> sentenceTerminators;

		bool setHyphenAsWordDivider;

		public TextContentAnalyzerResult(List<string> textBlocks, List<string> stopWords, bool setHyphenAsWordDivider)
		{
			this.sentenceTerminators = new List<char>() { '?', '.', '!', ';', ':' };
			this.setHyphenAsWordDivider = setHyphenAsWordDivider;

			this.textBlocks = textBlocks;
			this.textContent = string.Join(" ", this.textBlocks.Where(x => !string.IsNullOrWhiteSpace(x)).Select(x => sentenceTerminators.Contains(x.Last()) ? x : x + ".")).Trim();
			this.stopWords = stopWords;

			this.wordLists = new Dictionary<int, Dictionary<string, int>>();
			this.wordListsWithoutStopwords = new Dictionary<int, Dictionary<string, int>>();
		}

		public List<string> GetTextBlocks()
		{
			return this.textBlocks;
		}
		public string GetTextContent()
		{
			return this.textContent;
		}
		public int GetNumberOfChars()
		{
			return GetTextContent().Length;
		}
		public int GetNumberOfCharsWithoutWhitespace()
		{
			return Regex.Replace(GetTextContent(), @"[\s+]", "").Length;
		}
		public int GetNumberOfWords(bool removeStopwords = false)
		{
			if (!this.numberOfWords.HasValue)
			{
				this.numberOfWords = GetWordList(1, removeStopwords: removeStopwords).Sum(x => x.Value);
			}

			return this.numberOfWords.Value;
		}
		public int GetNumberOfDifferentWords()
		{
			if (!this.numberOfDifferentWords.HasValue)
			{
				this.numberOfDifferentWords = GetWordList(1).Count;
			}

			return this.numberOfDifferentWords.Value;
		}
		public int GetNumberOfWordsWithoutStopwords()
		{
			if (!this.numberOfWordswithoutStopwords.HasValue)
			{
				var wordlist = GetWordList(removeStopwords: true);
				this.numberOfWordswithoutStopwords = wordlist.Sum(x => x.Value);
			}

			return this.numberOfWordswithoutStopwords.Value;
		}
		public string GetTextContentOnlyWords()
		{
			if (this.textContentOnlyWords == null)
			{
				this.textContentOnlyWords = GetTextContent();

				this.textContentOnlyWords = Regex.Replace(this.textContentOnlyWords, @"[^\w\s-.',:€$]", " "); // Ersetzt alle Zeichen außer Wortzeichen, Leerzeichen und -.',: (-.', können Worttrenner sein, wie z.b. 6,90€ oder VHS-Kasette oder 4:3)

				if (this.setHyphenAsWordDivider)
				{
					this.textContentOnlyWords = Regex.Replace(this.textContentOnlyWords, @"-", " "); // Ersetzt den Hyphen, wenn die eingestellt wurde.
				}

				this.textContentOnlyWords = Regex.Replace(this.textContentOnlyWords, @"[^\w]*\s", " "); // Entfernt alle Nicht-Wortzeichen auf die ein Whitespace folgt und die somit keine Worttrenner sind
				this.textContentOnlyWords = Regex.Replace(this.textContentOnlyWords, @"[^\w]*$", " "); // Entfernt alle Nicht-Wortzeichen die am Ende des Strings stehen und die somit keine Worttrenner sind
				this.textContentOnlyWords = Regex.Replace(this.textContentOnlyWords, @"\s+", " "); // Ersetzt multible Whitespace-Character durch ein einziges Leerzeichen
				this.textContentOnlyWords = Regex.Replace(this.textContentOnlyWords, "[ ]+", " ");
			}

			return this.textContentOnlyWords;
		}
		public string GetTextContentWithoutStopwordsOnlyWords()
		{
			if (this.textContentOnlyWordsWithoutStopwords == null)
			{
				var content = GetTextContentOnlyWords();


				foreach (var item in this.stopWords)
				{
					content = Regex.Replace(" " + content + " ", " " + item + " ", " ", RegexOptions.IgnoreCase);
				}

				content = Regex.Replace(content, "[ ]+", " ", RegexOptions.Singleline | RegexOptions.IgnoreCase);
				this.textContentOnlyWordsWithoutStopwords = content.Trim();
			}

			return this.textContentOnlyWordsWithoutStopwords;
		}


		public Dictionary<string, int> GetWordList(int numberOfWords)
		{
			if (!this.wordLists.ContainsKey(numberOfWords))
			{
				var wordlist = GetWordList(GetTextContentOnlyWords(), numberOfWords);
				this.wordLists.Add(numberOfWords, wordlist);
			}

			return this.wordLists[numberOfWords];
		}
		public Dictionary<string, int> GetWordList(int numberOfWordsFrom = 1, int numberOfWordsTo = 1, bool removeStopwords = true)
		{
			var occurencesAll = new Dictionary<string, int>();

			var cacheDictionary = removeStopwords ? this.wordListsWithoutStopwords : this.wordLists;


			for (int i = numberOfWordsFrom; i <= numberOfWordsTo; i++)
			{
				if (!cacheDictionary.ContainsKey(i))
				{
					var wordlist = GetWordList(GetTextContentOnlyWords(), i);
					if (removeStopwords)
					{
						foreach (var sw in this.stopWords)
						{
							wordlist.Remove(sw);
						}
					}

					cacheDictionary.Add(i, wordlist);
				}

				var occurences = cacheDictionary[i];
				foreach (var item in occurences)
				{
					occurencesAll.Add(item.Key, item.Value);
				}
			}

			return occurencesAll;
		}
		public Dictionary<string, int> GetWordListWithoutStopwords(int numberOfWords)
		{
			if (!this.wordListsWithoutStopwords.ContainsKey(numberOfWords))
			{
				var wordlist = GetWordList(GetTextContentWithoutStopwordsOnlyWords(), numberOfWords);
				this.wordListsWithoutStopwords.Add(numberOfWords, wordlist);
			}

			return this.wordListsWithoutStopwords[numberOfWords];
		}




		public int GetNumberOfSyllables()
		{
			var content = this.textContent.ToLower();
			var vowels = new List<char>() { 'a', 'e', 'i', 'o', 'u', 'ö', 'ä', 'ü' };
			var result = 0;

			var lastWasVowel = false;
			for (int i = 0; i < content.Length; i++)
			{
				var vchar = content[i];
				if (vowels.Contains(vchar))
				{
					if (!lastWasVowel)
					{
						result++;
					}
					lastWasVowel = true;
				}
				else
				{
					lastWasVowel = false;
				}
			}

			return result;
		}
		public List<string> GetSentences()
		{
			string[] sentences = Regex.Split(this.textContent, @"(?<=[\.!\?;])\s+");

			return sentences.ToList();
		}
		public double GetFleschIndexGerman()
		{
			var numberOfSyllables = GetNumberOfSyllables();
			var numberOfSentences = GetSentences().Count;
			var numberOfWords = GetNumberOfWords();

			if (numberOfSentences == 0 || numberOfWords == 0)
			{
				return 0.0;
			}

			var averageSentencesLength = (double)numberOfWords / (double)numberOfSentences;
			var averageSyllablesPerWord = (double)numberOfSyllables / (double)numberOfWords;



			var result = 180.0 - averageSentencesLength - (58.5 * averageSyllablesPerWord);

			if (result < 0)
			{
				result = 0.0;
			}

			return result;
		}










		private Dictionary<string, int> GetWordList(string textContent, int numberOfWords)
		{
			textContent = textContent.ToLower();
			var wordlist = new Dictionary<string, int>();

			var words = textContent.Split(' ').Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
			for (int i = 0; i < words.Count - (1 - 1); i++)
			{
				if (i < (words.Count - (numberOfWords - 1)))
				{
					var wordPair = "";
					for (int j = 0; j < numberOfWords; j++)
					{
						wordPair += words[i + j] + " ";
					}
					wordPair = wordPair.Trim();

					if (!string.IsNullOrWhiteSpace(wordPair))
					{
						if (!wordlist.ContainsKey(wordPair))
						{
							wordlist.Add(wordPair, 0);
						}
						wordlist[wordPair]++;
					}
				}
			}

			return wordlist;
		}


	}
}
