﻿using System;
using System.Text;
using System.Text.RegularExpressions;

namespace IZY.SlySEO.Utilities.Logic
{
	public static class HelperMethods
    {
        public static string RemoveHtmlTags(string input)
        {
            return Regex.Replace(input, "<.*?>", " ");
        }
        public static string GetCleanContent(string content, bool removePunctuation = false)
        {
            content = RemoveHtmlTags(content);
            content = System.Net.WebUtility.HtmlDecode(content);

			byte[] data = Encoding.Default.GetBytes(content);
			content = Encoding.UTF8.GetString(data);

			// Entferne diverse Sonderzeichen
			content = Regex.Replace(content, @"[^a-zA-Z äöüÄÖÜß\.\?\!]", " ");

            if (removePunctuation)
            {
                content = Regex.Replace(content, @"[\.\?!]", " ");
            }

            // Entferne mehrfache Leerzeichen durch eines
            content = Regex.Replace(content, @"\s+", " ");

            return content.Trim().ToLower();
        }
        public static Uri TryCreateUri(string url)
        {
            try
            {
                return new Uri(url);
            }
            catch (Exception)
            {
                return null;
            }
        }
        public static bool IsInternalUrl(string domain, string url)
        {
            if (url.Contains(domain))
            {
                return true;
            }

            return false;
        }
		public static string GetFilePath(string path, string folderName, string fileName, string repository = "IZY.SlySEO.Utilities")
        {
            path = path.Replace(@"bin\Debug", String.Empty);
            path = Regex.Replace(path, "IZY.SlySEO.*", "IZY.SlySEO");
            path += @"\" + repository + @"\";
            path += @"\" + folderName + @"\";
            path += fileName;
            
            return path;
        }
        public static string GetDecodedUrl(string url)
        {
            var protocol = url.Contains("https") ? "https" : "http";

            url = Regex.Replace(url, ".*?" + protocol, protocol);
            url = Regex.Replace(url, "&amp;sa.*", String.Empty);

            url = System.Net.WebUtility.UrlDecode(url);

            return url;
        }
    }
}
