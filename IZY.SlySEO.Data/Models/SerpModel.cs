﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IZY.SlySEO.Data.Models
{
	public class SerpModel
	{
		public string SearchTerm { get; set; }
		public List<SnippetModel> Snippets { get; set; } = new List<SnippetModel>();
		public int Id { get; set; }
	}
}
