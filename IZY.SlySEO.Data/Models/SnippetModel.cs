﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IZY.SlySEO.Data.Models
{
	public class SnippetModel
	{
		public int Position { get; set; }
		public string UrlAsString { get; set; }
		public string ContentAsString { get; set; }
	}
}
