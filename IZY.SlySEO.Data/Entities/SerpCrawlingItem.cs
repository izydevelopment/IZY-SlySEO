//------------------------------------------------------------------------------
// <auto-generated>
//     Der Code wurde von einer Vorlage generiert.
//
//     Manuelle Änderungen an dieser Datei führen möglicherweise zu unerwartetem Verhalten der Anwendung.
//     Manuelle Änderungen an dieser Datei werden überschrieben, wenn der Code neu generiert wird.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IZY.SlySEO.Data.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class SerpCrawlingItem
    {
        public int Id { get; set; }
        public int SerpCrawlingId { get; set; }
        public int Position { get; set; }
        public string Url { get; set; }
        public string Html { get; set; }
    }
}
