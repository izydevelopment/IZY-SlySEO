﻿using IZY.SlySEO.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IZY.SlySEO.Data.DB
{
	public static class DBDomains
	{
		public static void Add(List<string> domains)
		{
			foreach (var domain in domains)
			{
				Add(domain);
			}
		}
		public static void Add(string domain)
		{
			if (!IsInDB(domain))
			{
				using (SlySEOEntities db = new SlySEOEntities())
				{
					var domainModel = new Domain()
					{
						Value = domain
					};

					db.Domains.Add(domainModel);

					db.SaveChanges();
				}
			}
		}

		public static int GetId(string domain)
		{
			int result;

			using (SlySEOEntities db = new SlySEOEntities())
			{

				result = (from t in db.Domains
						  where t.Value == domain
						  select t.Id).FirstOrDefault();

			}

			return result;
		}

		public static bool IsInDB(string domain)
		{
			if (GetId(domain) != 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
}
