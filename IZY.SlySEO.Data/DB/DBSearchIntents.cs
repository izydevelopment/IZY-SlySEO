﻿using IZY.SlySEO.Data.Entities;
using IZY.SlySEO.Models.Google;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IZY.SlySEO.Data.DB
{
	public static class DBSearchIntents
	{
		public static void Add(List<SearchIntentType> searchIntents)
		{
			using (SlySEOEntities db = new SlySEOEntities())
			{
				foreach (var searchIntent in searchIntents)
				{
					var searchIntentModel = new SearchIntent()
					{
						Value = searchIntent.ToString()
					};


					db.SearchIntents.Add(searchIntentModel);
				}

				db.SaveChanges();
			}
		}
		public static int GetId(SearchIntentType searchIntentType)
		{
			int result;

			using (SlySEOEntities db = new SlySEOEntities())
			{
				result = (from t in db.SearchIntents
						  where t.Value == searchIntentType.ToString()
						  select t.Id).FirstOrDefault();
			}

			return result;
		}
	}
}
