﻿using IZY.SlySEO.Data.Entities;
using IZY.SlySEO.Models.Google;
using System.Collections.Generic;
using System.Linq;

namespace IZY.SlySEO.Data.DB
{
    public static class DBSearchIntentTerms
    {
		public static void Initialize()
		{
			var searchIntentTerms = new Dictionary<string, SearchIntentType>();

			#region Informationell
			searchIntentTerms.Add("wikipedia", SearchIntentType.Informational);
			searchIntentTerms.Add("wikipedia.de", SearchIntentType.Informational);
			searchIntentTerms.Add("www.wikipedia.de", SearchIntentType.Informational);
			searchIntentTerms.Add("dictionary", SearchIntentType.Informational);
			searchIntentTerms.Add("lexikon", SearchIntentType.Informational);
			searchIntentTerms.Add("tutorial", SearchIntentType.Informational);
			searchIntentTerms.Add("geschichte", SearchIntentType.Informational);
			searchIntentTerms.Add("informationen", SearchIntentType.Informational);
			searchIntentTerms.Add("information", SearchIntentType.Informational);
			searchIntentTerms.Add("pons", SearchIntentType.Informational);
			searchIntentTerms.Add("duden", SearchIntentType.Informational);
			searchIntentTerms.Add("script", SearchIntentType.Informational);
			searchIntentTerms.Add("mitschrift", SearchIntentType.Informational);
			searchIntentTerms.Add("pdf", SearchIntentType.Informational);
			searchIntentTerms.Add("wiktionary", SearchIntentType.Informational);
			searchIntentTerms.Add("autor", SearchIntentType.Informational);
			searchIntentTerms.Add("rezept", SearchIntentType.Informational);
			searchIntentTerms.Add("neuigkeiten", SearchIntentType.Informational);
			searchIntentTerms.Add("blog", SearchIntentType.Informational);
			searchIntentTerms.Add("zeitung", SearchIntentType.Informational);
			searchIntentTerms.Add("magazin", SearchIntentType.Informational);
			searchIntentTerms.Add("news", SearchIntentType.Informational);
			searchIntentTerms.Add("fragen", SearchIntentType.Informational);
			searchIntentTerms.Add("infos", SearchIntentType.Informational);
			searchIntentTerms.Add("tipps", SearchIntentType.Informational);
			searchIntentTerms.Add("portal", SearchIntentType.Informational);
			searchIntentTerms.Add("teaser", SearchIntentType.Informational);
			searchIntentTerms.Add("hinweis", SearchIntentType.Informational);
			searchIntentTerms.Add("nachweis", SearchIntentType.Informational);
			searchIntentTerms.Add("zusammenfassung", SearchIntentType.Informational);
			searchIntentTerms.Add("erklärung", SearchIntentType.Informational);
			searchIntentTerms.Add("guide", SearchIntentType.Informational);
			searchIntentTerms.Add("newsletter", SearchIntentType.Informational);
			searchIntentTerms.Add("quelltext", SearchIntentType.Informational);
			searchIntentTerms.Add("warum", SearchIntentType.Informational);
			#endregion

			#region Transaktionell
			searchIntentTerms.Add("kaufen", SearchIntentType.Transactional);
			searchIntentTerms.Add("schenken", SearchIntentType.Transactional);
			searchIntentTerms.Add("download", SearchIntentType.Transactional);
			searchIntentTerms.Add("amazon", SearchIntentType.Transactional);
			searchIntentTerms.Add("amazon.de", SearchIntentType.Transactional);
			searchIntentTerms.Add("www.amazon.de", SearchIntentType.Transactional);
			searchIntentTerms.Add("amazon.com", SearchIntentType.Transactional);
			searchIntentTerms.Add("www.amazon.com", SearchIntentType.Transactional);
			searchIntentTerms.Add("testen", SearchIntentType.Transactional);
			searchIntentTerms.Add("angebot", SearchIntentType.Transactional);
			searchIntentTerms.Add("angebote", SearchIntentType.Transactional);
			searchIntentTerms.Add("reduziert", SearchIntentType.Transactional);
			searchIntentTerms.Add("günstig", SearchIntentType.Transactional);
			searchIntentTerms.Add("günstiger", SearchIntentType.Transactional);
			searchIntentTerms.Add("preis", SearchIntentType.Transactional);
			searchIntentTerms.Add("ebay", SearchIntentType.Transactional);
			searchIntentTerms.Add("ebay.de", SearchIntentType.Transactional);
			searchIntentTerms.Add("www.ebay.de", SearchIntentType.Transactional);
			searchIntentTerms.Add("kasse", SearchIntentType.Transactional);
			searchIntentTerms.Add("cart", SearchIntentType.Transactional);
			searchIntentTerms.Add("einkaufswagen", SearchIntentType.Transactional);
			searchIntentTerms.Add("shop", SearchIntentType.Transactional);
			searchIntentTerms.Add("händler", SearchIntentType.Transactional);
			searchIntentTerms.Add("verkauf", SearchIntentType.Transactional);
			searchIntentTerms.Add("kreditkarte", SearchIntentType.Transactional);
			searchIntentTerms.Add("bezahlung", SearchIntentType.Transactional);
			searchIntentTerms.Add("paypal", SearchIntentType.Transactional);
			searchIntentTerms.Add("paypal.com", SearchIntentType.Transactional);
			searchIntentTerms.Add("www.paypal.com", SearchIntentType.Transactional);
			searchIntentTerms.Add("zahlung", SearchIntentType.Transactional);
			searchIntentTerms.Add("dhl", SearchIntentType.Transactional);
			searchIntentTerms.Add("lieferungen", SearchIntentType.Transactional);
			searchIntentTerms.Add("rückerstattung", SearchIntentType.Transactional);
			searchIntentTerms.Add("lieferung", SearchIntentType.Transactional);
			searchIntentTerms.Add("liefern", SearchIntentType.Transactional);
			searchIntentTerms.Add("bestellen", SearchIntentType.Transactional);
			searchIntentTerms.Add("versand", SearchIntentType.Transactional);
			searchIntentTerms.Add("warenkorb", SearchIntentType.Transactional);
			searchIntentTerms.Add("verfügbarkeit", SearchIntentType.Transactional);
			searchIntentTerms.Add("verfügbar", SearchIntentType.Transactional);
			searchIntentTerms.Add("gebraucht", SearchIntentType.Transactional);
			searchIntentTerms.Add("merkzettel", SearchIntentType.Transactional);
			searchIntentTerms.Add("warenkörbe", SearchIntentType.Transactional);
			searchIntentTerms.Add("preise", SearchIntentType.Transactional);
			searchIntentTerms.Add("rückgaberecht", SearchIntentType.Transactional);
			searchIntentTerms.Add("kotanktlos", SearchIntentType.Transactional);
			searchIntentTerms.Add("kontaktloser", SearchIntentType.Transactional);
			searchIntentTerms.Add("rückversand", SearchIntentType.Transactional);
			searchIntentTerms.Add("marke", SearchIntentType.Transactional);
			searchIntentTerms.Add("otto", SearchIntentType.Transactional);
			searchIntentTerms.Add("otto.de", SearchIntentType.Transactional);
			searchIntentTerms.Add("www.otto.de", SearchIntentType.Transactional);
			searchIntentTerms.Add("einwilligung", SearchIntentType.Transactional);
			searchIntentTerms.Add("interessen", SearchIntentType.Transactional);
			searchIntentTerms.Add("mediamarkt", SearchIntentType.Transactional);
			searchIntentTerms.Add("mediamarkt.de", SearchIntentType.Transactional);
			searchIntentTerms.Add("www.mediamarkt.de", SearchIntentType.Transactional);
			searchIntentTerms.Add("verfügungen", SearchIntentType.Transactional);
			searchIntentTerms.Add("coupon", SearchIntentType.Transactional);
			searchIntentTerms.Add("coupon-code", SearchIntentType.Transactional);
			searchIntentTerms.Add("wearables", SearchIntentType.Transactional);
			searchIntentTerms.Add("sollzinssatz", SearchIntentType.Transactional);
			searchIntentTerms.Add("kreditrahmen", SearchIntentType.Transactional);
			searchIntentTerms.Add("neukunden", SearchIntentType.Transactional);
			searchIntentTerms.Add("guthaben", SearchIntentType.Transactional);
			searchIntentTerms.Add("märkten", SearchIntentType.Transactional);
			searchIntentTerms.Add("verträge", SearchIntentType.Transactional);
			searchIntentTerms.Add("vertrag", SearchIntentType.Transactional);
			searchIntentTerms.Add("mastercard", SearchIntentType.Transactional);
			searchIntentTerms.Add("vertragsschluss", SearchIntentType.Transactional);
			searchIntentTerms.Add("kombinierbar", SearchIntentType.Transactional);
			searchIntentTerms.Add("einlösbar", SearchIntentType.Transactional);
			searchIntentTerms.Add("willkommens-coupon", SearchIntentType.Transactional);
			searchIntentTerms.Add("nachlass", SearchIntentType.Transactional);
			searchIntentTerms.Add("mehrwert", SearchIntentType.Transactional);
			searchIntentTerms.Add("finanzierungspartner", SearchIntentType.Transactional);
			searchIntentTerms.Add("laufzeit", SearchIntentType.Transactional);
			searchIntentTerms.Add("monatliche", SearchIntentType.Transactional);
			searchIntentTerms.Add("monatlichen", SearchIntentType.Transactional);
			searchIntentTerms.Add("zoll", SearchIntentType.Transactional);
			searchIntentTerms.Add("jahreszins", SearchIntentType.Transactional);
			searchIntentTerms.Add("vorrat", SearchIntentType.Transactional);
			searchIntentTerms.Add("wartenwert", SearchIntentType.Transactional);
			searchIntentTerms.Add("gekauft", SearchIntentType.Transactional);
			#endregion

			#region Navigationell
			searchIntentTerms.Add("übersicht", SearchIntentType.Navigational);
			searchIntentTerms.Add("sammlung", SearchIntentType.Navigational);
			searchIntentTerms.Add("verzeichnis", SearchIntentType.Navigational);
			searchIntentTerms.Add("auflistung", SearchIntentType.Navigational);
			searchIntentTerms.Add("ansammlung", SearchIntentType.Navigational);
			searchIntentTerms.Add("liste", SearchIntentType.Navigational);
			searchIntentTerms.Add("navigation", SearchIntentType.Navigational);
			searchIntentTerms.Add("kategorien", SearchIntentType.Navigational);
			searchIntentTerms.Add("finden", SearchIntentType.Navigational);
			searchIntentTerms.Add("top", SearchIntentType.Navigational);
			searchIntentTerms.Add("search", SearchIntentType.Navigational);
			searchIntentTerms.Add("suche", SearchIntentType.Navigational);
			searchIntentTerms.Add("channel", SearchIntentType.Navigational);
			searchIntentTerms.Add("channels", SearchIntentType.Navigational);
			searchIntentTerms.Add("nav", SearchIntentType.Navigational);
			searchIntentTerms.Add("möglichkeiten", SearchIntentType.Navigational);
			#endregion

			Add(searchIntentTerms);

			var termMultipliers = new Dictionary<string, int>();

			#region Informationell
			termMultipliers.Add("wikipedia", 3);
			termMultipliers.Add("wikipedia.de", 3);
			termMultipliers.Add("tutorial", 3);
			termMultipliers.Add("geschichte", 3);
			termMultipliers.Add("rezept", 3);
			termMultipliers.Add("blog", 3);

			termMultipliers.Add("autor", 2);
			termMultipliers.Add("zeitung", 2);
			termMultipliers.Add("magazin", 2);
			termMultipliers.Add("zusammenfassung", 2);
			termMultipliers.Add("guide", 2);
			termMultipliers.Add("quelltext", 2);


			#endregion

			#region Transaktionell
			termMultipliers.Add("kaufen", 3);
			termMultipliers.Add("amazon", 3);
			termMultipliers.Add("amazon.de", 3);
			termMultipliers.Add("amazon.com", 3);
			termMultipliers.Add("preis", 3);
			termMultipliers.Add("kasse", 3);
			termMultipliers.Add("einkaufswagen", 3);
			termMultipliers.Add("shop", 3);
			termMultipliers.Add("warenkörbe", 3);
			termMultipliers.Add("preise", 3);
			termMultipliers.Add("warenwert", 3);
			termMultipliers.Add("warenkorb", 3);
			termMultipliers.Add("jahreszins", 3);

			termMultipliers.Add("angebot", 2);
			termMultipliers.Add("angebote", 2);
			termMultipliers.Add("reduziert", 2);
			termMultipliers.Add("zahlung", 2);
			termMultipliers.Add("neukunden", 2);
			termMultipliers.Add("guthaben", 2);
			termMultipliers.Add("zoll", 2);
			#endregion

			#region Navigationell
			termMultipliers.Add("übersicht", 3);
			termMultipliers.Add("sammlung", 3);
			termMultipliers.Add("auflistung", 3);
			termMultipliers.Add("finden", 3);
			termMultipliers.Add("navigation", 2);
			termMultipliers.Add("kategorien", 2);
			#endregion

			Update(termMultipliers);

		}
		public static void Add(Dictionary<string, SearchIntentType> searchIntentTerms)
		{
			DBTerms.Add(searchIntentTerms.Select(x => x.Key).ToList());

			using (SlySEOEntities db = new SlySEOEntities())
			{
				foreach (var searchIntentTerm in searchIntentTerms)
				{
					if (IsInDB(searchIntentTerm.Key, searchIntentTerm.Value))
					{
						continue;
					}

					var searchIntentId = (from si in db.SearchIntents
										  where si.Value == searchIntentTerm.Value.ToString()
										  select si).FirstOrDefault().Id;

					var termid = (from t in db.Terms
								  where t.Value == searchIntentTerm.Key.ToString()
								  select t).FirstOrDefault().Id;

					var newSearchIntentTerm = new SearchIntentTerm()
					{
						SearchIntentId = searchIntentId,
						TermId = termid,
						Multiplier = 1
					};

					db.SearchIntentTerms.Add(newSearchIntentTerm);
				}

				db.SaveChanges();
			}

		}
		public static void Update(Dictionary<string, int> termMulitpliers)
		{
			foreach (var termMulitplier in termMulitpliers)
			{
				Update(termMulitplier.Key, termMulitplier.Value);
			}
		}
		public static void Update(string term, int multiplier)
		{
			using (SlySEOEntities db = new SlySEOEntities())
			{

				var result = (from sit in db.SearchIntentTerms
							  join t in db.Terms on sit.TermId equals t.Id
							  where t.Value == term
							  select sit).FirstOrDefault();

				if(result != null)
				{
					result.Multiplier = multiplier;
					db.SaveChanges();
				}

			}
		}
		public static void Delete(string term)
		{
			using (SlySEOEntities db = new SlySEOEntities())
			{

				var result = (from sit in db.SearchIntentTerms
							  join t in db.Terms on sit.TermId equals t.Id
							  where t.Value == term
							  select sit).FirstOrDefault();

				if (result != null)
				{
					db.SearchIntentTerms.Remove(result);
					db.SaveChanges();
				}

			}
		}
		public static Dictionary<string, int> Get(SearchIntentType searchIntentType, bool useDatabase = true)
        {
            var result = new Dictionary<string, int>();

            if (useDatabase)
            {
                result = Get(searchIntentType);
            }
            else
            {
                if (searchIntentType == SearchIntentType.Informational)
                {
                    result = GetInformational();
                }
                if (searchIntentType == SearchIntentType.Transactional)
                {
                    result = GetTransactinoal();
                }
                if (searchIntentType == SearchIntentType.Navigational)
                {
                    result = GetNavigational();
                }
            }

            return result;
        }
		public static Dictionary<string, int> Get(SearchIntentType searchIntentType)
		{
			var result = new Dictionary<string, int>();

			using (SlySEOEntities db = new SlySEOEntities())
			{

				result = (from sic in db.SearchIntentTerms
						  join t in db.Terms on sic.TermId equals t.Id
						  where (SearchIntentType)sic.SearchIntentId == searchIntentType
						  select new
						  {
							  Value = t.Value,
							  Multiplier = sic.Multiplier
						  }).ToDictionary(x => x.Value, x => x.Multiplier);
			}

			return result;
		}
		private static Dictionary<string, int> GetInformational()
        {
            var result = new Dictionary<string, int>();

            result.Add("wikipedia", 1);
            result.Add("wikipedia.de", 1);
            result.Add("dictionary", 1);
            result.Add("lexikon", 1);
            result.Add("tutorial", 1);
            result.Add("geschichte", 1);
            result.Add("informationen", 1);
            result.Add("information", 1);
            result.Add("pons", 1);
            result.Add("duden", 1);
            result.Add("script", 1);
            result.Add("mitschrift", 1);
            result.Add("pdf", 1);
            result.Add("wiktionary", 1);
            result.Add("autor", 1);
            result.Add("rezept", 1);
            result.Add("neuigkeiten", 1);
            result.Add("blog", 1);
            result.Add("zeitung", 1);
            result.Add("magazin", 1);
            result.Add("news", 1);
            result.Add("fragen", 1);
            result.Add("infos", 1);
            result.Add("tipps", 1);
            result.Add("portal", 1);
            result.Add("teaser", 1);
            result.Add("hinweis", 1);
            result.Add("nachweis", 1);
            result.Add("zusammenfassung", 1);
            result.Add("erklärung", 1);
            result.Add("guide", 1);
            result.Add("newsletter", 1);
			result.Add("quelltext", 1);
			result.Add("warum", 1);

			return result;
        }
        private static Dictionary<string, int> GetTransactinoal()
        {
            var result = new Dictionary<string, int>();

            result.Add("kaufen", 1);
            result.Add("schenken", 1);
            result.Add("download", 1);
            result.Add("amazon", 1);
            result.Add("amazon.de", 1);
            result.Add("amazon.com", 1);
            result.Add("testen", 1);
            result.Add("angebot", 1);
			result.Add("angebote", 1);
			result.Add("reduziert", 1);
            result.Add("günstig", 1);
            result.Add("günstiger", 1);
            result.Add("preis", 1);
            result.Add("ebay",1);
            result.Add("ebay.de", 1);
			result.Add("www.ebay.de", 1);
			result.Add("kasse", 1);
            result.Add("cart", 1);
            result.Add("einkaufswagen", 1);
            result.Add("shop", 1);
            result.Add("händler", 1);
            result.Add("verkauf", 1);
            result.Add("kreditkarte", 1);
            result.Add("bezahlung", 1);
            result.Add("paypal", 1);
            result.Add("paypal.com", 1);
            result.Add("zahlung", 1);
            result.Add("dhl", 1);
            result.Add("lieferungen", 1);
            result.Add("rückerstattung", 1);
            result.Add("lieferung", 1);
            result.Add("liefern", 1);
            result.Add("bestellen", 1);
            result.Add("versand", 1);
			result.Add("warenkorb", 1);
			result.Add("verfügbarkeit", 1);
			result.Add("verfügbar", 1);
			result.Add("gebraucht", 1);
			result.Add("merkzettel", 1);

			return result;
        }
        private static Dictionary<string, int> GetNavigational()
        {
            var result = new Dictionary<string, int>();

            result.Add("übersicht", 1);
            result.Add("sammlung", 1);
            result.Add("verzeichnis", 1);
            result.Add("auflistung", 1);
            result.Add("ansammlung", 1);
            result.Add("liste", 1);
            result.Add("navigation", 1);
			result.Add("kategorien", 1);
			result.Add("finden", 1);
			result.Add("top", 1);

			return result;
        }
		public static bool IsInDB(string term, SearchIntentType searchIntentType)
		{

			if (DBTerms.GetId(term) == 0)
			{
				return false;
			}

			using (SlySEOEntities db = new SlySEOEntities())
			{
				var query = (from sit in db.SearchIntentTerms
							 join t in db.Terms on sit.TermId equals t.Id
							 where (SearchIntentType)sit.SearchIntentId == searchIntentType && t.Value == term
							 select new
							 {
								 Term = t.Value,
								 SearchIntent = (SearchIntentType)sit.SearchIntentId
							 });

				if (query.Count() == 0)
				{
					return false;
				}

			}

			return true; ;
		}
	}
}
