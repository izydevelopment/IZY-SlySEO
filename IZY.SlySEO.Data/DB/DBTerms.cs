﻿using IZY.SlySEO.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IZY.SlySEO.Data.DB
{
	public static class DBTerms
	{
		public static void Add(string term)
		{
			if (!IsInDB(term))
			{
				using (SlySEOEntities db = new SlySEOEntities())
				{
					var termModel = new Term()
					{
						Value = term
					};

					db.Terms.Add(termModel);

					db.SaveChanges();
				}
			}
		}
		public static void Add(List<string> terms)
		{
			using (SlySEOEntities db = new SlySEOEntities())
			{
				foreach (var term in terms)
				{
					if (!IsInDB(term))
					{
						var termModel = new Term()
						{
							Value = term
						};

						db.Terms.Add(termModel);
					}
				}

				db.SaveChanges();
			}
		}
		public static void Delete(string term)
		{
			using (SlySEOEntities db = new SlySEOEntities())
			{

				var result = (from t in db.Terms
							  where t.Value == term
							  select t).FirstOrDefault();

				if (result != null)
				{
					db.Terms.Remove(result);
					db.SaveChanges();

					DBSearchIntentTerms.Delete(term);
				}

			}
		}
		public static int? GetId(string term)
		{
			int result;

			using (SlySEOEntities db = new SlySEOEntities())
			{

				result = (from t in db.Terms
						  where t.Value == term
						  select t.Id).FirstOrDefault();

			}

			return result;
		}
		public static bool IsInDB(string term)
		{
			if (GetId(term) != 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
}
