﻿using IZY.SlySEO.Data.Entities;
using IZY.SlySEO.Models.Google;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IZY.SlySEO.Data.DB
{
	public static class DBSearchIntentCrawlings
	{
		public static void Add(SearchIntentResult searchIntentResult)
		{
			if (!DBTerms.IsInDB(searchIntentResult.SearchTerm))
			{
				DBTerms.Add(searchIntentResult.SearchTerm);
			}
			if (IsInDB(searchIntentResult.SearchTerm))
			{
				Update(searchIntentResult);
			}
			else
			{
				using (SlySEOEntities db = new SlySEOEntities())
				{

					var searchIntentId = (from si in db.SearchIntents
										  where si.Value == searchIntentResult.Winner.ToString()
										  select si).FirstOrDefault().Id;

					var termId = (from t in db.Terms
								  where t.Value == searchIntentResult.SearchTerm
								  select t).FirstOrDefault().Id;

					var searchIntentCrawling = new SearchIntentCrawling()
					{
						DateTime = DateTime.Now,
						TermId = termId,
						SearchIntentId = searchIntentId,
						AnalyzedUrlsCount = searchIntentResult.AnalyzedUrlsCount,
						TransactionalScore = searchIntentResult.TransactionalScore,
						InformationalScore = searchIntentResult.InformationalScore,
						NavigationalScore = searchIntentResult.NavigationalScore,
						ProbabilityPercentage = searchIntentResult.ProbabilityPercentage
					};


					db.SearchIntentCrawlings.Add(searchIntentCrawling);
					db.SaveChanges();

					DBSearchIntentCrawlingItems.Add(searchIntentCrawling.Id, searchIntentResult.AnalyzedUrls);
				}
			}
		}
		public static void Update(SearchIntentResult searchIntentResult)
		{
			if (!IsInDB(searchIntentResult.SearchTerm))
			{
				return;
			}

			var searchIntentCrawlingId = GetId(searchIntentResult.SearchTerm);

			using (SlySEOEntities db = new SlySEOEntities())
			{
				var searchIntentCrawling = db.SearchIntentCrawlings.Where(x => x.Id == searchIntentCrawlingId).FirstOrDefault();

				var searchIntentId = DBSearchIntents.GetId(searchIntentResult.Winner);

				searchIntentCrawling.DateTime = DateTime.Now;
				searchIntentCrawling.SearchIntentId = searchIntentId;
				searchIntentCrawling.InformationalScore = searchIntentCrawling.InformationalScore;
				searchIntentCrawling.NavigationalScore = searchIntentCrawling.NavigationalScore;
				searchIntentCrawling.TransactionalScore = searchIntentCrawling.TransactionalScore;
				searchIntentCrawling.ProbabilityPercentage = searchIntentCrawling.ProbabilityPercentage;
				db.SaveChanges();

				DBSearchIntentCrawlingItems.Delete(searchIntentCrawlingId);
				DBSearchIntentCrawlingItems.Add(searchIntentCrawlingId, searchIntentResult.AnalyzedUrls);
			}
		}
		public static List<SearchIntentResult> Get(string searchTerm)
		{
			var result = new List<SearchIntentResult>();

			using (SlySEOEntities db = new SlySEOEntities())
			{
				var searchIntentCrawlingId = DBSearchIntentCrawlings.GetId(searchTerm);

				result = (from sic in db.SearchIntentCrawlings
						  join t in db.Terms on sic.TermId equals t.Id
						  join si in db.SearchIntents on sic.SearchIntentId equals si.Id
						  where sic.Id == searchIntentCrawlingId
						  select new SearchIntentResult
						  {
							  SearchTerm = t.Value,
							  AnalyzedUrlsCount = sic.AnalyzedUrlsCount,
							  AnalyzedUrls = (from sici in db.SearchIntentCrawlingItems
											  join si in db.SearchIntents on sici.SearchIntentId equals si.Id
											  where sici.SearchIntentCrawlingId == searchIntentCrawlingId
											  select new SearchIntentItem
											  {
												  Url = sici.Url,
												  Position = sici.Position,
												  InformationalScore = sici.InformationalScore,
												  NavigationalScore = sici.NavigationalScore,
												  TransactionalScore = sici.TransactionalScore,
												  Winner = (SearchIntentType)si.Id
											  }).ToList(),
							  InformationalScore = sic.InformationalScore,
							  NavigationalScore = sic.NavigationalScore,
							  TransactionalScore = sic.TransactionalScore,
							  ProbabilityPercentage = sic.ProbabilityPercentage,
							  Winner = (SearchIntentType)si.Id
						  }).ToList();
			}

			return result;
		}
		public static List<SearchIntentResult> Get()
		{
			var result = new List<SearchIntentResult>();

			using (SlySEOEntities db = new SlySEOEntities())
			{
				result = (from sic in db.SearchIntentCrawlings
						  join t in db.Terms on sic.TermId equals t.Id
						  join si in db.SearchIntents on sic.SearchIntentId equals si.Id
						  select new SearchIntentResult
						  {
							  SearchTerm = t.Value,
							  AnalyzedUrlsCount = sic.AnalyzedUrlsCount,
							  AnalyzedUrls = (from sici in db.SearchIntentCrawlingItems
											  join si2 in db.SearchIntents on sici.SearchIntentId equals si2.Id
											  where sici.SearchIntentCrawlingId == sic.Id
											  select new SearchIntentItem
											  {
												  Url = sici.Url,
												  Position = sici.Position,
												  InformationalScore = sici.InformationalScore,
												  NavigationalScore = sici.NavigationalScore,
												  TransactionalScore = sici.TransactionalScore,
												  Winner = (SearchIntentType)si.Id
											  }).ToList(),
							  InformationalScore = sic.InformationalScore,
							  NavigationalScore = sic.NavigationalScore,
							  TransactionalScore = sic.TransactionalScore,
							  ProbabilityPercentage = sic.ProbabilityPercentage,
							  Winner = (SearchIntentType)si.Id
						  }).ToList();
			}

			return result;
		}
		public static int GetId(string searchTerm)
		{
			int result;

			using (SlySEOEntities db = new SlySEOEntities())
			{

				result = (from sic in db.SearchIntentCrawlings
						  join t in db.Terms on sic.TermId equals t.Id
						  where t.Value == searchTerm.ToLower()
						  select sic.Id).FirstOrDefault();

			}

			return result;
		}
		public static bool IsInDB(string searchTerm)
		{
			using (SlySEOEntities db = new SlySEOEntities())
			{


				var query = (from sic in db.SearchIntentCrawlings
							 join t in db.Terms on sic.TermId equals t.Id
							 where t.Value == searchTerm
							 select sic).ToList();

				if (query.Count() == 0)
				{
					return false;
				}

			}

			return true;
		}
	}
}
