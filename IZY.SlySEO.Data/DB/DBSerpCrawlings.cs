﻿using IZY.SlySEO.Data.Entities;
using IZY.SlySEO.Data.Models;
using IZY.SlySEO.Models.Google;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IZY.SlySEO.Data.DB
{
	public static class DBSerpCrawlings
	{
		public static void Add(GoogleSerp googleSerp)
		{
			if (!DBTerms.IsInDB(googleSerp.SearchTerm))
			{
				DBTerms.Add(googleSerp.SearchTerm);
			}
			if (IsInDB(googleSerp.SearchTerm))
			{
				Update(googleSerp);
			}
			else
			{
				using (SlySEOEntities db = new SlySEOEntities())
				{
					var termId = (from t in db.Terms
								  where t.Value == googleSerp.SearchTerm
								  select t).FirstOrDefault().Id;

					var searchIntentCrawling = new SerpCrawling()
					{
						DateTime = DateTime.Now,
						TermId = termId,
						AnalyzedUrlsCount = googleSerp.Snippets.Count(),
					};


					db.SerpCrawlings.Add(searchIntentCrawling);
					db.SaveChanges();

					DBSerpCrawlingItems.Add(searchIntentCrawling.Id, googleSerp.Snippets);
				}
			}	
		}
		public static void Update(GoogleSerp googleSerp)
		{
			if (!IsInDB(googleSerp.SearchTerm))
			{
				return;
			}

			var serpCrawlingId = GetId(googleSerp.SearchTerm);

			using (SlySEOEntities db = new SlySEOEntities())
			{
				var serpCrawling = db.SerpCrawlings.Where(x => x.Id == serpCrawlingId).FirstOrDefault();

				serpCrawling.DateTime = DateTime.Now;
				serpCrawling.AnalyzedUrlsCount = googleSerp.Snippets.Count();

				db.SaveChanges();

				DBSerpCrawlingItems.Delete(serpCrawlingId);
				DBSerpCrawlingItems.Add(serpCrawlingId, googleSerp.Snippets);
			}
		}
		public static List<SerpModel> Get(string searchTerm)
		{
			var result = new List<SerpModel>();

			using (SlySEOEntities db = new SlySEOEntities())
			{

				var termId = DBTerms.GetId(searchTerm);

				result = (from sc in db.SerpCrawlings
						  join t in db.Terms on sc.TermId equals t.Id
						  where t.Id == termId
						  select new SerpModel
						  {
							  Id = sc.Id,
							  SearchTerm = t.Value,
							  Snippets = (from sci in db.SerpCrawlingItems
										  where sci.SerpCrawlingId == sc.Id
										  select new SnippetModel
										  {
											  UrlAsString = sci.Url,
											  Position = sci.Position,
											  ContentAsString = sci.Html
										  }).ToList()
						  }).ToList();
			}

			return result;
		}
		public static List<SerpModel> Get()
		{
			var result = new List<SerpModel>();

			using (SlySEOEntities db = new SlySEOEntities())
			{


				result = (from sc in db.SerpCrawlings
						  join t in db.Terms on sc.TermId equals t.Id
						  select new SerpModel
						  {
							  Id = sc.Id,
							  SearchTerm = t.Value,
							  Snippets = (from sci in db.SerpCrawlingItems
										  where sci.SerpCrawlingId == sc.Id
										  select new SnippetModel
										  {
											  UrlAsString = sci.Url,
											  Position = sci.Position,
											  ContentAsString = sci.Html
										  }).ToList()
						  }).ToList();
			}

			return result;
		}
		public static int GetId(string searchTerm)
		{
			int result;

			using (SlySEOEntities db = new SlySEOEntities())
			{

				result = (from sic in db.SerpCrawlings
						  join t in db.Terms on sic.TermId equals t.Id
						  where t.Value == searchTerm.ToLower()
						  select sic.Id).FirstOrDefault();

			}

			return result;
		}
		public static bool IsInDB(string searchTerm)
		{
			using (SlySEOEntities db = new SlySEOEntities())
			{
				var query = (from sc in db.SerpCrawlings
							 join t in db.Terms on sc.TermId equals t.Id
							 where t.Value == searchTerm
							 select sc).ToList();

				if (query.Count() == 0)
				{
					return false;
				}

			}

			return true;
		}
	}
}
