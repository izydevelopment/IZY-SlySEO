﻿using IZY.SlySEO.Data.Entities;
using IZY.SlySEO.Models.Google;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IZY.SlySEO.Data.DB
{
	public static class DBSerpCrawlingItems
	{
		public static void Add(int serpCrawlingId, List<Snippet> snippets)
		{
			using (SlySEOEntities db = new SlySEOEntities())
			{
				foreach (var snippet in snippets)
				{
					var serpCrawlingItem = new SerpCrawlingItem()
					{
						SerpCrawlingId = serpCrawlingId,
						Position = snippet.Position,
						Url = snippet.UrlAsString,
						Html = snippet.ContentAsString,
					};


					db.SerpCrawlingItems.Add(serpCrawlingItem);
				}

				db.SaveChanges();
			}
		}
		public static void Delete(int serpCrawlingId)
		{
			using (SlySEOEntities db = new SlySEOEntities())
			{
				var itemsToRemove = db.SerpCrawlingItems.Where(x => x.SerpCrawlingId == serpCrawlingId).ToList();

				foreach (var itemToRemove in itemsToRemove)
				{
					db.SerpCrawlingItems.Remove(itemToRemove);
				}

				db.SaveChanges();
			}
		}
	}
}
