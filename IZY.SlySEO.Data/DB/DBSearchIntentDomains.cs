﻿using IZY.SlySEO.Data.Entities;
using IZY.SlySEO.Models.Google;
using IZY.SlySEO.Utilities.Logic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IZY.SlySEO.Data.DB
{
	public static class DBSearchIntentDomains
	{
		public static void Initialize()
		{
			var result = new Dictionary<string, SearchIntentType>();

			#region Read From File
			string path = HelperMethods.GetFilePath(Directory.GetCurrentDirectory(), "DB", "Domains.txt", "IZY.SlySEO.Data");

			foreach (var line in File.ReadLines(path))
			{
				var values = line.Split(';');

				if(values.Count() > 1)
				{
					int type = Convert.ToInt32(values[1]);
					result.Add(values[0], (SearchIntentType)type);
				}
				else
				{
					result.Add(values[0], SearchIntentType.None);
				}
				
			}
			#endregion

			Add(result);
		}
		public static void Add(Dictionary<string, SearchIntentType> searchIntentDomains)
		{
			DBDomains.Add(searchIntentDomains.Select(x => x.Key).ToList());

			using (SlySEOEntities db = new SlySEOEntities())
			{
				foreach (var searchIntentDomain in searchIntentDomains)
				{
					if (IsInDB(searchIntentDomain.Key, searchIntentDomain.Value))
					{
						continue;
					}

					var searchIntentId = (from si in db.SearchIntents
										  where si.Value == searchIntentDomain.Value.ToString()
										  select si).FirstOrDefault().Id;

					var domainId = (from t in db.Domains
								  where t.Value == searchIntentDomain.Key.ToString()
								  select t).FirstOrDefault().Id;

					var newSearchIntentDomain = new SearchIntentDomain()
					{
						SearchIntentId = searchIntentId,
						DomainId = domainId
					};

					db.SearchIntentDomains.Add(newSearchIntentDomain);
				}

				db.SaveChanges();
			}
		}

		public static void Update(string domain, SearchIntentType searchIntentType)
		{
			if (!IsInDB(domain, searchIntentType))
			{
				return;
			}

			var searchIntentDomainId = GetId(domain);

			using (SlySEOEntities db = new SlySEOEntities())
			{
				var searchIntentDomain = db.SearchIntentDomains.Where(x => x.Id == searchIntentDomainId).FirstOrDefault();

				var searchIntentId = DBSearchIntents.GetId(searchIntentType);

				searchIntentDomain.SearchIntentId = searchIntentId;

				db.SaveChanges();
			}
		}
		public static Dictionary<string, SearchIntentType> Get(bool useDatabase = true)
		{
			var result = new Dictionary<string, SearchIntentType>();

			if (useDatabase)
			{
				result = Get();
			}
			else
			{
				#region Read From File
				string path = HelperMethods.GetFilePath(Directory.GetCurrentDirectory(), "DB", "Domains.txt", "IZY.SlySEO.Data");

				foreach (var line in File.ReadLines(path))
				{
					var values = line.Split(';');

					if (values.Count() > 1)
					{
						int type = Convert.ToInt32(values[1]);
						result.Add(values[0], (SearchIntentType)type);
					}
					else
					{
						result.Add(values[0], SearchIntentType.None);
					}

				}
				#endregion
			}

			return result;
		}

		public static Dictionary<string, SearchIntentType> Get()
		{
			var result = new Dictionary<string, SearchIntentType>();

			using (SlySEOEntities db = new SlySEOEntities())
			{

				result = (from sid in db.SearchIntentDomains
						  join t in db.Domains on sid.DomainId equals t.Id
						  select new
						  {
							  Value = t.Value,
							  SearchIntentType = (SearchIntentType)sid.SearchIntentId
						  }).ToDictionary(x => x.Value, x => x.SearchIntentType);
			}

			return result;
		}

		public static Dictionary<string, SearchIntentType> Get(SearchIntentType searchIntentType)
		{
			var result = new Dictionary<string, SearchIntentType>();

			using (SlySEOEntities db = new SlySEOEntities())
			{

				result = (from sid in db.SearchIntentDomains
						  join t in db.Domains on sid.DomainId equals t.Id
						  where (SearchIntentType)sid.SearchIntentId == searchIntentType
						  select new
						  {
							  Value = t.Value,
							  SearchIntentType = (SearchIntentType)sid.SearchIntentId
						  }).ToDictionary(x => x.Value, x => x.SearchIntentType);
			}

			return result;
		}

		public static int GetId(string domain)
		{
			int result;

			using (SlySEOEntities db = new SlySEOEntities())
			{

				result = (from sic in db.SearchIntentDomains
						  join t in db.Domains on sic.DomainId equals t.Id
						  where t.Value == domain.ToLower()
						  select sic.Id).FirstOrDefault();

			}

			return result;
		}

		public static bool IsInDB(string domain, SearchIntentType searchIntentType)
		{

			if (DBDomains.GetId(domain) == 0)
			{
				return false;
			}

			using (SlySEOEntities db = new SlySEOEntities())
			{
				var query = (from sid in db.SearchIntentDomains
							 join t in db.Terms on sid.SearchIntentId equals t.Id
							 where (SearchIntentType)sid.SearchIntentId == searchIntentType && t.Value == domain
							 select new
							 {
								 Domain = t.Value,
								 SearchIntent = (SearchIntentType)sid.SearchIntentId
							 });

				if (query.Count() == 0)
				{
					return false;
				}

			}

			return true; ;
		}
	}
}
