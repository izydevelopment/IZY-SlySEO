﻿using IZY.SlySEO.Utilities.Logic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IZY.SlySEO.Data.DB
{
	public static class DBStopwords
	{

		public static List<string> GetGerman()
		{
			var result = new List<string>();

			string path = HelperMethods.GetFilePath(Directory.GetCurrentDirectory(), "Stopwords", "GermanStopwords.csv");

			string content = File.ReadAllText(path);

			result = content.Split(' ').Select(x => x.TrimEnd(',')).ToList();

			return result;
		}
		public static List<string> GetVariableGerman()
		{
			var result = new List<string>();

			string path = HelperMethods.GetFilePath(Directory.GetCurrentDirectory(), "Stopwords", "VariableGermanStopwords.csv");

			string content = File.ReadAllText(path);

			result = content.Split(' ').Select(x => x.TrimEnd(',')).ToList();

			return result;
		}
	}
}
