﻿using IZY.SlySEO.Data.Entities;
using IZY.SlySEO.Models.Google;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IZY.SlySEO.Data.DB
{
	public static class DBSearchIntentCrawlingItems
	{
		public static void Add(int searchIntentCrawlingId, List<SearchIntentItem> searchIntentItems)
		{
			using (SlySEOEntities db = new SlySEOEntities())
			{
				foreach (var searchIntentItem in searchIntentItems)
				{
					var searchIntentId = (from si in db.SearchIntents
										  where si.Value == searchIntentItem.Winner.ToString()
										  select si).FirstOrDefault().Id;


					var searchIntentCrawlingItem = new SearchIntentCrawlingItem()
					{
						SearchIntentCrawlingId = searchIntentCrawlingId,
						Position = searchIntentItem.Position,
						Url = searchIntentItem.Url,
						SearchIntentId = searchIntentId,
						InformationalScore = searchIntentItem.InformationalScore,
						TransactionalScore = searchIntentItem.TransactionalScore,
						NavigationalScore = searchIntentItem.NavigationalScore,
					};


					db.SearchIntentCrawlingItems.Add(searchIntentCrawlingItem);
				}


				db.SaveChanges();
			}

		}
		public static void Delete(int searchIntentCrawlingId)
		{
			using (SlySEOEntities db = new SlySEOEntities())
			{
				var itemsToRemove = db.SearchIntentCrawlingItems.Where(x => x.SearchIntentCrawlingId == searchIntentCrawlingId).ToList();

				foreach (var itemToRemove in itemsToRemove)
				{
					db.SearchIntentCrawlingItems.Remove(itemToRemove);
				}		

				db.SaveChanges();
			}
		}
	}
}
